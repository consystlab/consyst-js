var DeclarationBundlerPlugin = require('declaration-bundler-webpack-plugin');

const babelOptions = {
  presets: ['es2015', 'stage-0'],
  sourceMaps: true,
  retainLines: true,
};

module.exports = {
  entry: {
    consyst: './source/index.ts',
  },
  output: {
    path: `${__dirname}/dist`,
    filename: '[name].js',
    library: '[name]',
    libraryTarget: 'commonjs2',
    publicPath: '/dist/',
  },
  devtool: 'source-map',
  resolve: {
    modules: ['source/', 'node_modules/'],
    extensions: ['.ts', '/index.ts'],
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [
          {
            loader: 'babel-loader',
            options: babelOptions,
          }, {
            loader: 'ts-loader',
          },
        ],
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new DeclarationBundlerPlugin({
      moduleName:'consyst',
      out:'./consyst.d.ts',
    })
  ],
  node: {
    global: true,
  },
};
