# ConSyst-JS

A Constraint Solver written in TypeScript for Sudoku Puzzles. It provides
two constraint models, binary and non-binary, five (5) consistency
algorithms for each model, as well as an exhaustive backtrack-search
procedure with two types of lookahead for each model.

### Reference Materials

Typescript Modules: https://www.typescriptlang.org/docs/handbook/modules.html

### Papers

* Problem Formulation: http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.88.2964
* GAC: https://www.aaai.org/Papers/AAAI/1994/AAAI94-055.pdf
* SAC: http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.54.4126
* POAC: https://link.springer.com/chapter/10.1007%2F3-540-45578-7_39
* BiSAC: https://www.sciencedirect.com/science/article/pii/S0004370207001282
* System published in the Demo Track of IJCAI 2018: https://www.ijcai.org/proceedings/2018/0852.pdf