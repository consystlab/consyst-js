import { SearchResults } from 'Enumerations';
import LexicographicalOrdering from 'orderings/variable/LexicographicalOrdering';
import IVariableOrdering from 'orderings/variable/IVariableOrdering';
import VariableOrdering from 'orderings/variable/VariableOrdering';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import Wrapper from 'structures/Wrapper';
import SearchAnalytics from 'structures/SearchAnalytics';

import ISearch from 'searches/ISearch';

const searchAnalytics = SearchAnalytics.getInstance();

function bcssp(
  sudokuRef: Sudoku,
  SearchMethodBuilder: (
    sudoku: Sudoku,
    variableOrdering?: VariableOrdering
  ) => ISearch,
  VariableOrderingConstructor: IVariableOrdering,
  singleSolution: boolean = true,
  maxSolutions: number = 50,
  cleanCopy: boolean = false,
): [
  SearchResults,
  Array<{ [key: string]: [Variable, number] }>,
  SearchAnalytics
] {
  const sudoku = Sudoku.copy(sudokuRef);
  const variableList = sudoku.variables;

  if (cleanCopy) {
    variableList.forEach(variable => {
      if (!variable.isAssigned()) {
        variable.domain.restore();
      }
    });
  }

  if (!sudoku.verifyConsistencies()) {
    return [SearchResults.INCONSISTENT_BEFORE_SEARCH, [], searchAnalytics];
  }

  searchAnalytics.initialize(variableList.length);
  searchAnalytics.start();

  const variableOrdering = new VariableOrderingConstructor(sudoku);
  const searchMethod = SearchMethodBuilder(sudoku, variableOrdering);
  if (!searchMethod) {
    return [SearchResults.INCONSISTENT_BEFORE_SEARCH, [], searchAnalytics];
  }

  const consistent = new Wrapper<boolean>(true);
  let status = SearchResults.UNKNOWN;
  let i = 1;
  const numberOfVariables = variableList.length;
  const solutions = new Array<{ [key: string]: [Variable, number] }>();

  while (i > 0) {
    if (consistent.get()) {
      // searchAnalytics.nodesVisited++;
      i = searchMethod.label(i, consistent);
    } else {
      searchAnalytics.backtracks++;
      searchAnalytics.backtracksPerLevel[i]++;
      i = searchMethod.unlabel(i, consistent);
    }

    if (i > numberOfVariables) {
      status = SearchResults.SOLUTIONS;

      const solution: { [key: string]: [Variable, number] } = {};
      variableList.forEach(variable => {
        solution[variable.id] = [variable, variable.domain.first()];
      });
      solutions.push(solution);

      if (singleSolution) {
        break;
      }

      i = searchMethod.handleSolution(i, consistent);

      if (solutions.length > maxSolutions) {
        status = SearchResults.TOO_MANY;
        break;
      }
    }
  }
  searchAnalytics.stop();
  if (status === SearchResults.UNKNOWN) {
    status = SearchResults.IMPOSSIBLE;
  }
  return [status, solutions, searchAnalytics];
}

export default bcssp;
