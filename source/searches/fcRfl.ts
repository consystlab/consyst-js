import IDomain from 'domains/IDomain';
import ValuesRemoved from 'structures/ValuesRemoved';
import Queue from 'structures/Queue';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import Wrapper from 'structures/Wrapper';
import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import SearchAnalytics from 'structures/SearchAnalytics';
import VariableOrdering from 'orderings/variable/VariableOrdering';

import ISearch from 'searches/ISearch';

const searchAnalytics = SearchAnalytics.getInstance();

// Returns changed variables and the values removed from their domains
const fcGacSparse = (
  sudoku: Sudoku,
  variableOrdering: VariableOrdering,
): ISearch => {
  const uninstantiated = sudoku.variables.slice();
  const path = Array<Variable>();
  for (let i = 0; i < uninstantiated.length; i++) {
    path[i + 1] = uninstantiated[i];
  }

  const reductions: { [key: string]: Array<Set<number>> } = {};
  const futureFc: { [key: string]: Set<Variable> } = {};
  const pastFc: { [key: string]: Set<Variable> } = {};

  // Calculate initial reductions and start the incremental diff.
  sudoku.binaryArcConsistency();

  const initialReductions = new ValuesRemoved();
  sudoku.variables.forEach(variable => {
    const reduction = new Set<number>();
    if (variable.isAssigned()) {
      const assignment = variable.getAssignment();
      variable.unassign();
      variable.domain.original().forEach(value => {
        if (value !== assignment) {
          reduction.add(value);
          variable.domain.remove(value);
        }
      });
    } else {
      variable.domain.original().forEach(value => {
        if (!variable.domain.contains(value)) {
          reduction.add(value);
          variable.domain.remove(value);
        }
      });
    }
    initialReductions.setRemoved(variable, reduction);
  });
  initialReductions.forEach((variable, removed) => {
    if (!reductions[variable.id]) {
      reductions[variable.id] = new Array<Set<number>>();
    }
    reductions[variable.id].push(removed);
  });

  const checkForward = (current: Variable): boolean => {
    const totalRemoved = new ValuesRemoved();
    let consistent = true;
    const queue = new Queue<[Variable, Variable]>();
    sudoku.variables.forEach(var1 => {
      if (var1.isAssigned() || var1.domain.size() === 1) {
        var1.binaryNeighbors.forEach(var2 => {
          queue.push([var2, var1]);
        });
      }
    });
    while (!queue.isEmpty() && consistent) {
      const tuple = queue.pop();
      const original = new Set<number>(tuple[0].domain.current());
      if (tuple[0].binaryRevise(tuple[1])) {
        tuple[0].domain.current().forEach(val => {
          if (original.has(val)) {
            original.delete(val);
          }
        });

        if (!totalRemoved.getRemoved(tuple[0]) && original.size > 0) {
          totalRemoved.initialize(tuple[0]);
        }

        const removedValues = new Set<number>();
        original.forEach(val => {
          totalRemoved.addRemoved(tuple[0], val);
          removedValues.add(val);
        });

        if (tuple[0].domain.size() === 1) {
          tuple[0].binaryNeighbors.forEach(neighbor => {
            queue.push([neighbor, tuple[0]]);
          });
        } else if (!tuple[0].domain.size()) {
          consistent = false;
          break;
        }
      }
    }

    totalRemoved.forEach((variable, removed) => {
      if (!reductions[variable.id]) {
        reductions[variable.id] = new Array<Set<number>>();
      }
      reductions[variable.id].push(removed);

      if (!futureFc[current.id]) {
        futureFc[current.id] = new Set<Variable>();
      }
      futureFc[current.id].add(variable);

      if (!pastFc[variable.id]) {
        pastFc[variable.id] = new Set<Variable>();
      }
      pastFc[variable.id].add(current);
    });
    return consistent;
  };

  const undoReductions = (current: Variable) => {
    if (futureFc[current.id]) {
      futureFc[current.id].forEach(variable => {
        const reduction = reductions[variable.id].pop();
        reduction.forEach(value => restore(variable, value));
        pastFc[variable.id].delete(current);
      });
      futureFc[current.id] = null;
    }
  };

  const updateCurrentDomain = (current: Variable) => {
    restoreAll(current);
    if (reductions[current.id]) {
      reductions[current.id].forEach(reduction => {
        reduction.forEach(value => remove(current, value));
      });
    }
  };

  const placeNextVariable = (i: number) => {
    const temp = path[i];
    const next = variableOrdering.next();
    if (!next) {
      return false;
    }
    path[i] = next;
    for (let j = i + 1; j < path.length; j++) {
      if (path[j] === next) {
        path[j] = temp;
        break;
      }
    }
    return true;
  };

  const removeSearchedDomain = (i: number, value: number) => {
    removeSearchedDomains(i, new Set<number>([value]));
  };

  const removeSearchedDomains = (i: number, values: Set<number>) => {
    // Add removed values to the reduction of path[i - 1] on path[i]
    if (path[i - 1]) {
      let reduction = new Set<number>();
      if (!pastFc[path[i].id]) {
        pastFc[path[i].id] = new Set<Variable>();
      }
      if (pastFc[path[i].id].has(path[i - 1])) {
        reduction = reductions[path[i].id].pop();
      } else {
        if (!futureFc[path[i - 1].id]) {
          futureFc[path[i - 1].id] = new Set<Variable>();
        }
        futureFc[path[i - 1].id].add(path[i]);
        if (!pastFc[path[i].id]) {
          pastFc[path[i].id] = new Set<Variable>();
        }
        pastFc[path[i].id].add(path[i - 1]);
      }
      values.forEach(val => reduction.add(val));
      reductions[path[i].id].push(reduction);
    }
  };

  const label = (i: number, consistent: Wrapper<boolean>): number => {
    consistent.set(false);
    if (!placeNextVariable(i)) {
      return 0;
    }

    const removed = new Set<number>();
    while (path[i].domain.size() !== 0 && !consistent.get()) {
      consistent.set(true);
      const assignment = path[i].domain.first();
      assign(path[i], assignment);
      searchAnalytics.nodesVisited++;
      consistent.set(checkForward(path[i]));

      if (!consistent.get()) {
        unassign(path[i]);
        remove(path[i], assignment);
        removed.add(assignment);
        undoReductions(path[i]);
      }
    }

    if (removed.size) {
      removeSearchedDomains(i, removed);
    }

    if (consistent.get()) {
      return i + 1;
    }
    variableOrdering.restore();
    return i;
  };

  const unlabel = (i: number, consistent: Wrapper<boolean>): number => {
    const h = i - 1;
    if (h > 0) {
      const assignedValue = path[h].domain.first();
      undoReductions(path[h]);
      updateCurrentDomain(path[i]);
      unassign(path[h]);
      remove(path[h], assignedValue);
      removeSearchedDomain(h, assignedValue);
      consistent.set(path[h].domain.size() !== 0);
    } else {
      consistent.set(false);
    }
    variableOrdering.restore();
    return h;
  };

  const handleSolution = (i: number, consistent: Wrapper<boolean>): number => {
    const x = i - 1;
    const removal = path[x].domain.first();
    unassign(path[x]);
    remove(path[x], removal);
    removeSearchedDomain(x, removal);
    consistent.set(path[x].domain.size() !== 0);
    variableOrdering.restore();
    return x;
  };

  return {
    handleSolution,
    label,
    unlabel,
  };
};

const assign = (variable: Variable, value: number) => {
  variable.domain.current().forEach(removal => {
    if (removal !== value) {
      variable.generalConstraints.forEach(c => c.remove(variable, removal));
    }
  });
  variable.assign(value);
};

const unassign = (variable: Variable) => {
  variable.unassign();
};

const remove = (variable: Variable, value: number): void => {
  variable.domain.remove(value);
};

const restore = (variable: Variable, value: number): void => {
  variable.domain.restore(value);
};

const restoreAll = (variable: Variable): void => {
  variable.domain.restore();
};

export default fcGacSparse;
