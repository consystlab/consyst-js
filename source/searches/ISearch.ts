import SearchResult from 'structures/SearchResult';
import Variable from 'Variable';
import Wrapper from 'structures/Wrapper';

interface ISearch {
  label: (
    i: number,
    consistent: Wrapper<boolean>,
  ) => number;

  unlabel: (
    i: number,
    consistent: Wrapper<boolean>,
  ) => number;

  handleSolution: (
    i: number,
    consistent: Wrapper<boolean>,
  ) => number;
}

export default ISearch;
