import BinaryAllDiff from 'constraints/binary/BinaryAllDiff';
import LexicographicalOrdering from 'orderings/variable/LexicographicalOrdering';
import LexicoSingletonOrdering from 'orderings/variable/LexicoSingletonOrdering';
import VariableOrdering from 'orderings/variable/VariableOrdering';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import BCSSP from 'searches/bcssp';
import FCRFL from 'searches/fcRfl';
import SparseSet from 'domains/SparseSet';
import { SearchResults } from 'Enumerations';

describe('FC-RFL', () => {
  let variables: Array<Variable>;
  let constraints: Array<BinaryAllDiff>;
  let sudoku: Sudoku;

  it('Finds no solutions to the two element domain triangle', () => {
    variables = [
      new Variable(new SparseSet([1, 2]), 'a'),
      new Variable(new SparseSet([1, 2]), 'b'),
      new Variable(new SparseSet([1, 2]), 'c'),
    ];
    constraints = new Array<BinaryAllDiff>(
      new BinaryAllDiff([variables[0], variables[1]]),
      new BinaryAllDiff([variables[1], variables[2]]),
      new BinaryAllDiff([variables[0], variables[2]])
    );
    sudoku = new Sudoku(variables, constraints);

    const solutionResult = BCSSP(
      sudoku,
      FCRFL,
      LexicoSingletonOrdering,
      false,
    );

    expect(solutionResult[0]).toEqual(SearchResults.IMPOSSIBLE);
  });

  it('Finds two solutions to a path', () => {
    variables = [
      new Variable(new SparseSet([1, 2]), 'a'),
      new Variable(new SparseSet([1]), 'b'),
      new Variable(new SparseSet([1, 2]), 'c'),
    ];
    constraints = new Array<BinaryAllDiff>(
      new BinaryAllDiff([variables[0], variables[2]]),
    );
    sudoku = new Sudoku(variables, constraints);

    const solutionResult = BCSSP(
      sudoku,
      FCRFL,
      LexicographicalOrdering,
      false,
    );

    expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
    expect(solutionResult[1].length).toEqual(2);
  });

  it('Finds 4 solutions with a free value', () => {
    variables = [
      new Variable(new SparseSet([1, 2]), 'a'),
      new Variable(new SparseSet([1]), 'b'),
      new Variable(new SparseSet([1, 2, 3]), 'c'),
    ];
    constraints = new Array<BinaryAllDiff>(
      new BinaryAllDiff([variables[0], variables[2]]),
    );
    sudoku = new Sudoku(variables, constraints);

    const solutionResult = BCSSP(
      sudoku,
      FCRFL,
      LexicographicalOrdering,
      false,
    );

    expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
    expect(solutionResult[1].length).toEqual(4);
  });

  it('Works when copied', () => {
    variables = [
      new Variable(new SparseSet([1, 2]), 'a'),
      new Variable(new SparseSet([1]), 'b'),
      new Variable(new SparseSet([1, 2, 3]), 'c'),
    ];
    constraints = new Array<BinaryAllDiff>(
      new BinaryAllDiff([variables[0], variables[2]]),
    );
    sudoku = new Sudoku(variables, constraints);

    const solutionResult = BCSSP(
      sudoku,
      FCRFL,
      LexicographicalOrdering,
      false,
      50,
      true
    );

    expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
    expect(solutionResult[1].length).toEqual(4);
  });

  it('Stops at a single solution when copied', () => {
    variables = [
      new Variable(new SparseSet([1, 2]), 'a'),
      new Variable(new SparseSet([1]), 'b'),
      new Variable(new SparseSet([1, 2, 3]), 'c'),
    ];
    constraints = new Array<BinaryAllDiff>(
      new BinaryAllDiff([variables[0], variables[2]]),
    );
    sudoku = new Sudoku(variables, constraints);

    const solutionResult = BCSSP(
      sudoku,
      FCRFL,
      LexicographicalOrdering,
      true,
      1,
      true
    );

    expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
    expect(solutionResult[1].length).toEqual(1);
  });

  it('Responds correctly to assignments', () => {
    variables = [
      new Variable(new SparseSet([1, 2]), 'a'),
      new Variable(new SparseSet([1]), 'b'),
      new Variable(new SparseSet([1, 2, 3]), 'c'),
    ];
    constraints = new Array<BinaryAllDiff>(
      new BinaryAllDiff([variables[0], variables[2]]),
    );

    variables[2].assign(2);
    sudoku = new Sudoku(variables, constraints);

    const solutionResult = BCSSP(
      sudoku,
      FCRFL,
      LexicographicalOrdering,
      false
    );

    expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
    expect(solutionResult[1].length).toEqual(1);
  });

  it('Backtracks correctly', () => {
    variables = [
      new Variable(new SparseSet([1, 2, 3]), 'a'),
      new Variable(new SparseSet([1, 2, 3]), 'b'),
      new Variable(new SparseSet([1, 2, 3]), 'c'),
      new Variable(new SparseSet([1, 2, 3]), 'd'),
    ];
    constraints = new Array<BinaryAllDiff>(
      new BinaryAllDiff([variables[0], variables[1]]),
      new BinaryAllDiff([variables[0], variables[2]]),
      new BinaryAllDiff([variables[0], variables[3]]),

      new BinaryAllDiff([variables[1], variables[2]]),
      new BinaryAllDiff([variables[1], variables[3]]),
      new BinaryAllDiff([variables[2], variables[3]]),
    );

    sudoku = new Sudoku(variables, constraints);

    const solutionResult = BCSSP(
      sudoku,
      FCRFL,
      LexicographicalOrdering,
      false
    );

    expect(solutionResult[0]).toEqual(SearchResults.IMPOSSIBLE);
    expect(solutionResult[1].length).toEqual(0);
  });

  // it('Responds correctly to assignment and problem copying', () => {
  //   variables = [
  //     new Variable(new SparseSet([1, 2, 3]), 'a'),
  //     new Variable(new SparseSet([1, 2, 3]), 'b'),
  //     new Variable(new SparseSet([1, 2, 3]), 'c'),

  //     new Variable(new SparseSet([1, 2, 3]), 'd'),
  //     new Variable(new SparseSet([1, 2, 3]), 'e'),
  //     new Variable(new SparseSet([1, 2, 3]), 'f'),

  //     new Variable(new SparseSet([1, 2, 3]), 'g'),
  //     new Variable(new SparseSet([1, 2, 3]), 'h'),
  //     new Variable(new SparseSet([1, 2, 3]), 'i'),
  //   ];
  //   constraints = [
  //     new BinaryAllDiff([variables[0], variables[1]]),
  //     new BinaryAllDiff([variables[0], variables[2]]),
  //     new BinaryAllDiff([variables[1], variables[2]]),

  //     new BinaryAllDiff([variables[3], variables[4]]),
  //     new BinaryAllDiff([variables[3], variables[5]]),
  //     new BinaryAllDiff([variables[4], variables[5]]),

  //     new BinaryAllDiff([variables[6], variables[7]]),
  //     new BinaryAllDiff([variables[6], variables[8]]),
  //     new BinaryAllDiff([variables[7], variables[8]]),

  //     new BinaryAllDiff([variables[0], variables[3]]),
  //     new BinaryAllDiff([variables[0], variables[6]]),
  //     new BinaryAllDiff([variables[3], variables[6]]),

  //     new BinaryAllDiff([variables[1], variables[4]]),
  //     new BinaryAllDiff([variables[1], variables[7]]),
  //     new BinaryAllDiff([variables[4], variables[7]]),

  //     new BinaryAllDiff([variables[2], variables[5]]),
  //     new BinaryAllDiff([variables[2], variables[8]]),
  //     new BinaryAllDiff([variables[5], variables[8]]),
  //   ];

  //   variables[0].assign(1);
  //   sudoku = new Sudoku(variables, constraints);

  //   const solutionResult = BCSSP(
  //     sudoku,
  //     FC,
  //     LexicoSingletonOrdering,
  //     true,
  //     1,
  //     true
  //   );

  //   expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
  //   expect(solutionResult[1].length).toEqual(4);
  //   expect(solutionResult[2].nodesVisited).toEqual(8);
  //   expect(solutionResult[2].backtracks).toEqual(5);
  //   expect(solutionResult[2].backtracksPerLevel).toEqual([0, 1, 2, 2]);
  // });
});
