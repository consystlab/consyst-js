import GeneralAlldiff from 'constraints/general/GeneralAllDiff';
import DomWDegOrdering from 'orderings/variable/DomWDegOrdering';
import LexicographicalOrdering from 'orderings/variable/LexicographicalOrdering';
import VariableOrdering from 'orderings/variable/VariableOrdering';
import SparseSet from 'domains/SparseSet';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import BCSSP from 'searches/bcssp';
import FCGAC from 'searches/fcGac';
import { SearchResults } from 'Enumerations';

describe('FcGac', () => {
  let variables: Array<Variable>;
  let constraints: Array<GeneralAlldiff>;
  let sudoku: Sudoku;

  it('Finds no solutions to the two element domain triangle', () => {
    variables = [
      new Variable(new SparseSet([1, 2]), 'a'),
      new Variable(new SparseSet([1, 2]), 'b'),
      new Variable(new SparseSet([1, 2]), 'c'),
    ];
    constraints = new Array<GeneralAlldiff>(
      new GeneralAlldiff([variables[0], variables[1]]),
      new GeneralAlldiff([variables[1], variables[2]]),
      new GeneralAlldiff([variables[0], variables[2]])
    );
    sudoku = new Sudoku(variables, [], constraints);

    const solutionResult = BCSSP(
      sudoku,
      FCGAC,
      DomWDegOrdering,
      false,
    );

    expect(solutionResult[0]).toEqual(SearchResults.IMPOSSIBLE);
    expect(solutionResult[2].constraintChecks).toEqual(0);
  });

  it('Finds two solutions to a path', () => {
    variables = [
      new Variable(new SparseSet([1, 2]), 'a'),
      new Variable(new SparseSet([1]), 'b'),
      new Variable(new SparseSet([1, 2]), 'c'),
    ];
    constraints = new Array<GeneralAlldiff>(
      new GeneralAlldiff([variables[0], variables[2]]),
    );
    sudoku = new Sudoku(variables, [], constraints);

    const solutionResult = BCSSP(
      sudoku,
      FCGAC,
      DomWDegOrdering,
      false,
    );

    expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
    expect(solutionResult[1].length).toEqual(2);
    expect(solutionResult[2].constraintChecks).toEqual(0);
  });

  it('Finds 4 solutions with a free value', () => {
    variables = [
      new Variable(new SparseSet([1, 2]), 'a'),
      new Variable(new SparseSet([1]), 'b'),
      new Variable(new SparseSet([1, 2, 3]), 'c'),
    ];
    constraints = new Array<GeneralAlldiff>(
      new GeneralAlldiff([variables[0], variables[2]]),
    );
    sudoku = new Sudoku(variables, [], constraints);

    const solutionResult = BCSSP(
      sudoku,
      FCGAC,
      DomWDegOrdering,
      false,
    );

    expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
    expect(solutionResult[1].length).toEqual(4);
    expect(solutionResult[2].constraintChecks).toEqual(0);
  });

  it('Finds 4 solutions with a free value with lexico ordering', () => {
    variables = [
      new Variable(new SparseSet([1, 2]), 'a'),
      new Variable(new SparseSet([1]), 'b'),
      new Variable(new SparseSet([1, 2, 3]), 'c'),
    ];
    constraints = new Array<GeneralAlldiff>(
      new GeneralAlldiff([variables[0], variables[2]]),
    );
    sudoku = new Sudoku(variables, [], constraints);

    const solutionResult = BCSSP(
      sudoku,
      FCGAC,
      LexicographicalOrdering,
      false,
    );

    expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
    expect(solutionResult[1].length).toEqual(4);
    expect(solutionResult[2].constraintChecks).toEqual(0);
  });

  it('Finds lots of solutions', () => {
    variables = [
      new Variable(new SparseSet([1, 2]), 'a'),
      new Variable(new SparseSet([1, 2]), 'b'),
      new Variable(new SparseSet([1, 2]), 'c'),
      new Variable(new SparseSet([1, 2]), 'd'),
    ];
    constraints = new Array<GeneralAlldiff>();
    sudoku = new Sudoku(variables, [], constraints);

    const solutionResult = BCSSP(
      sudoku,
      FCGAC,
      LexicographicalOrdering,
      false,
    );

    expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
    expect(solutionResult[1].length).toEqual(16);
    expect(solutionResult[2].constraintChecks).toEqual(0);
  });

  it('Fails a search gracefully', () => {
    variables = [
      new Variable(new SparseSet([1, 2]), 'c'),
      new Variable(new SparseSet([1, 2, 3]), 'a'),
      new Variable(new SparseSet([1, 2, 3]), 'b'),
    ];
    constraints = new Array<GeneralAlldiff>(
      new GeneralAlldiff([variables[0], variables[1]]),
      new GeneralAlldiff([variables[1], variables[2]]),
      new GeneralAlldiff([variables[0], variables[2]])
    );
    sudoku = new Sudoku(variables, [], constraints);

    variables[2].assign(1);
    variables[1].assign(2);

    const solutionResult = BCSSP(
      sudoku,
      FCGAC,
      DomWDegOrdering,
      false,
    );

    expect(solutionResult[0]).toEqual(SearchResults.INCONSISTENT_BEFORE_SEARCH);
    expect(solutionResult[1].length).toEqual(0);
  });
});
