import IDomain from 'domains/IDomain';
import ValuesRemoved from 'structures/ValuesRemoved';
import Queue from 'structures/Queue';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import Wrapper from 'structures/Wrapper';
import SearchAnalytics from 'structures/SearchAnalytics';

import VariableOrdering from 'orderings/variable/VariableOrdering';

import ISearch from 'searches/ISearch';

const searchAnalytics = SearchAnalytics.getInstance();

// Returns changed variables and the values removed from their domains
const fc = (
  sudoku: Sudoku,
  variableOrdering: VariableOrdering,
): ISearch => {
  const uninstantiated = sudoku.variables.slice();
  const path = Array<Variable>();
  for (let i = 0; i < uninstantiated.length; i++) {
    path[i + 1] = uninstantiated[i];
  }

  const reductions: { [key: string]: Array<Set<number>> } = {};
  const futureFc: { [key: string]: Set<Variable> } = {};
  const pastFc: { [key: string]: Set<Variable> } = {};

  sudoku.binaryArcConsistency();

  // Calculate initial reductions
  sudoku.variables.forEach(variable => {
    const reduction = new Set<number>();
    if (variable.isAssigned()) {
      const assignment = variable.getAssignment();
      variable.unassign();
      variable.domain.original().forEach(value => {
        if (value !== assignment) {
          reduction.add(value);
          variable.domain.remove(value);
        }
      });
    } else {
      variable.domain.original().forEach(value => {
        if (!variable.domain.contains(value)) {
          reduction.add(value);
        }
      });
    }
    if (!reductions[variable.id]) {
      reductions[variable.id] = new Array<Set<number>>();
    }
    reductions[variable.id].push(reduction);
  });

  const checkForward = (current: Variable, next: Variable): boolean => {
    const reduction = new Set<number>();

    next.domain.current().forEach(value => {
      if (!next.binarySupported(value, current)) {
        reduction.add(value);
      }
    });

    if (reduction.size > 0) {
      reduction.forEach(value => {
        next.domain.remove(value);
      });

      if (!reductions[next.id]) {
        reductions[next.id] = new Array<Set<number>>();
      }
      reductions[next.id].push(reduction);

      if (!futureFc[current.id]) {
        futureFc[current.id] = new Set<Variable>();
      }
      futureFc[current.id].add(next);

      if (!pastFc[next.id]) {
        pastFc[next.id] = new Set<Variable>();
      }
      pastFc[next.id].add(current);
    }
    return next.domain.size() > 0;
  };

  const undoReductions = (current: Variable) => {
    if (futureFc[current.id]) {
      futureFc[current.id].forEach(variable => {
        const reduction = reductions[variable.id].pop();
        reduction.forEach(value => { variable.domain.restore(value); });
        pastFc[variable.id].delete(current);
      });
      futureFc[current.id] = null;
    }
  };

  const updateCurrentDomain = (current: Variable) => {
    current.domain.restore();
    if (reductions[current.id]) {
      reductions[current.id].forEach(reduction => {
        reduction.forEach(value => { current.domain.remove(value); });
      });
    }
  };

  const placeNextVariable = (i: number) => {
    const temp = path[i];
    const next = variableOrdering.next();
    if (!next) {
      return false;
    }
    path[i] = next;
    for (let j = i + 1; j < path.length; j++) {
      if (path[j] === next) {
        path[j] = temp;
        break;
      }
    }
    return true;
  };

  const removeSearchedDomain = (i: number, value: number) => {
    removeSearchedDomains(i, new Set<number>([value]));
  };

  const removeSearchedDomains = (i: number, values: Set<number>) => {
    // Add removed values to the reduction of path[i - 1] on path[i]
    if (path[i - 1]) {
      let reduction = new Set<number>();
      if (!pastFc[path[i].id]) {
        pastFc[path[i].id] = new Set<Variable>();
      }
      if (pastFc[path[i].id].has(path[i - 1])) {
        reduction = reductions[path[i].id].pop();
      } else {
        if (!futureFc[path[i - 1].id]) {
          futureFc[path[i - 1].id] = new Set<Variable>();
        }
        futureFc[path[i - 1].id].add(path[i]);
        if (!pastFc[path[i].id]) {
          pastFc[path[i].id] = new Set<Variable>();
        }
        pastFc[path[i].id].add(path[i - 1]);
      }
      values.forEach(val => reduction.add(val));
      reductions[path[i].id].push(reduction);
    }
  };

  const label = (i: number, consistent: Wrapper<boolean>): number => {
    consistent.set(false);
    if (!placeNextVariable(i)) {
      return 0;
    }

    const removed = new Set<number>();
    while (path[i].domain.size() !== 0 && !consistent.get()) {
      consistent.set(true);
      path[i].assign(path[i].domain.first());
      searchAnalytics.nodesVisited++;
      for (let j = i + 1; j < path.length; j++) {
        consistent.set(checkForward(path[i], path[j]));
        updateCurrentDomain(path[j]);

        if (!consistent.get()) {
          const removal = path[i].getAssignment();
          path[i].unassign();
          path[i].domain.remove(removal);
          removed.add(removal);
          undoReductions(path[i]);
          break;
        }
      }
    }

    if (removed.size) {
      removeSearchedDomains(i, removed);
    }

    if (consistent.get()) {
      return i + 1;
    }
    variableOrdering.restore();
    return i;
  };

  const unlabel = (i: number, consistent: Wrapper<boolean>): number => {
    const h = i - 1;
    if (h > 0) {
      undoReductions(path[h]);
      updateCurrentDomain(path[i]);
      const assignment = path[h].getAssignment();
      path[h].unassign();
      path[h].domain.remove(assignment);
      removeSearchedDomain(h, assignment);
      consistent.set(path[h].domain.size() !== 0);
    } else {
      consistent.set(false);
    }
    variableOrdering.restore();
    return h;
  };

  const handleSolution = (i: number, consistent: Wrapper<boolean>): number => {
    const x = i - 1;
    const removal = path[x].getAssignment();
    path[x].unassign();
    path[x].domain.remove(removal);
    removeSearchedDomain(x, removal);
    consistent.set(path[x].domain.size() !== 0);
    variableOrdering.restore();
    return x;
  };

  return {
    handleSolution,
    label,
    unlabel,
  };
};

export default fc;
