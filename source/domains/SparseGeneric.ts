import BaseDomain from 'domains/BaseDomain';

class SparseSetGeneric<T> {
  private domD: T[] = [];
  private mapD: { [key: string]: number} = {};
  private sizeD: number = 0;

  public constructor(values: T[]) {
    this.domD = values;
    this.mapD = {};
    for (let i = 0; i < this.domD.length; i++) {
      this.mapD[this.domD[i].toString()] = i;
    }
    this.sizeD = values.length;
  }

  public contains(value: T): boolean {
    return this.mapD[value.toString()] < this.sizeD;
  }

  public copy(): SparseSetGeneric<T> {
    const newSs = new SparseSetGeneric<T>(this.domD.slice());
    newSs.mapD = Object.assign({}, this.mapD);

    newSs.sizeD = this.sizeD;
    return newSs;
  }

  public current(): T[] {
    return this.domD.slice(0, this.sizeD);
  }

  public first(): T {
    if (this.sizeD === 0) {
      return null;
    }
    return this.domD[0];
  }

  public original(): T[] {
    return this.domD.slice();
  }

  public remove(value: T): boolean {
    if (!this.contains(value)) {
      return false;
    }
    if (this.mapD[value.toString()] < this.sizeD) {
      this.swap(this.mapD[value.toString()], this.sizeD - 1);
      this.sizeD -= 1;
    }
    return true;
  }

  public restore(value?: T): boolean {
    if (value != null) {
      if (this.mapD[value.toString()] >= this.sizeD) {
        this.swap(this.mapD[value.toString()], this.sizeD);
        this.sizeD += 1;
        return true;
      }
    } else {
      this.sizeD = this.domD.length;
      return true;
    }
    return false;
  }

  public restoreRange(value: number): boolean {
    if (this.sizeD + value <= this.domD.length) {
      this.sizeD += value;
    }
    return false;
  }

  public size(): number {
    return this.sizeD;
  }

  protected get(value: number): T {
    return this.domD[value];
  }

  private swap(i: number, j: number): void {
    if (i >= this.domD.length || j >= this.domD.length || i < 0 || j < 0) {
      throw Error('Swap out of range: domSize: ' + this.domD.length + ' :i: ' + i + ' :j: ' + j);
    }
    const temp = this.domD[i];
    this.domD[i] = this.domD[j];
    this.domD[j] = temp;
    this.mapD[this.domD[j].toString()] = j;
    this.mapD[this.domD[i].toString()] = i;
  }

  private bind(v: T): void {
    if (this.mapD[v.toString()] == null) {
      throw Error('Bind::Element not in domD called: ' + v);
    }
    if (this.mapD[v.toString()] >= this.sizeD) {
      this.sizeD = 0;
    } else {
      this.swap(this.mapD[v.toString()], this.sizeD - 1);
      this.sizeD -= 1;
    }
  }
}

export default SparseSetGeneric;
