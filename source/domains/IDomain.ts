interface IDomain {
  contains(value: number): boolean;
  current(): number[];
  first(): number;
  original(): number[];
  remove(value: number): boolean;
  restore(value?: number): void;
  size(): number;

  startTemp(): void;
  resetTemp(): void;
  endTemp(): void;

  copy(): IDomain;
}

export default IDomain;
