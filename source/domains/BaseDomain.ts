import IDomain from 'domains/IDomain';

abstract class BaseDomain implements IDomain {
  public abstract contains(value: number): boolean;
  public abstract current(): number[];
  public abstract first(): number;
  public abstract original(): number[];
  public abstract remove(value: number): boolean;
  public abstract restore(value?: number): void;
  public abstract size(): number;

  public abstract startTemp(): void;
  public abstract resetTemp(): void;
  public abstract endTemp(): void;

  public abstract copy(): IDomain;
}

export default BaseDomain;
