import BaseDomain from 'domains/BaseDomain';

class SparseSet extends BaseDomain {
  private domD: number[] = [];
  private mapD: { [key: number]: number} = {};
  private sizeD: number = 0;
  private tempSizeD: number = 0;
  private temp: boolean = false;

  public constructor(values: number[]) {
    super();

    this.temp = false;
    this.domD = values;
    this.mapD = {};
    for (let i = 0; i < this.domD.length; i++) {
      this.mapD[this.domD[i]] = i;
    }
    this.tempSizeD = values.length;
    this.sizeD = values.length;
  }

  public contains(value: number): boolean {
    if (this.temp) {
      return this.mapD[value] < this.tempSizeD;
    }
    return this.mapD[value] < this.sizeD;
  }

  public copy(): SparseSet {
    const newSs = new SparseSet(this.domD.slice());
    newSs.mapD = Object.assign({}, this.mapD);

    newSs.sizeD = this.sizeD;

    newSs.temp = this.temp;
    newSs.tempSizeD = this.tempSizeD;

    return newSs;
  }

  public current(): number[] {
    if (this.temp) {
      return this.domD.slice(0, this.tempSizeD);
    }
    return this.domD.slice(0, this.sizeD);
  }

  public first(): number {
    if (this.temp && this.tempSizeD === 0) {
      return -1;
    } else if (this.sizeD === 0) {
      return -1;
    }
    return this.domD[0];
  }

  public original(): number[] {
    return this.domD.slice();
  }

  public remove(value: number): boolean {
    if (!this.contains(value)) {
      return false;
    }
    if (this.temp) {
      if (this.mapD[value] < this.tempSizeD) {
        this.swap(this.mapD[value], this.tempSizeD - 1);
        this.tempSizeD -= 1;
      }
    } else {
      if (this.mapD[value] < this.sizeD) {
        this.swap(this.mapD[value], this.sizeD - 1);
        this.sizeD -= 1;
      }
    }
    return true;
  }

  public restore(value?: number): void {
    if (this.temp) {
      if (value != null) {
        if (this.mapD[value] >= this.tempSizeD) {
          this.swap(this.mapD[value], this.tempSizeD);
          this.tempSizeD += 1;
        }
      } else {
        this.tempSizeD = this.sizeD;
      }
    } else {
      if (value != null) {
        if (this.mapD[value] >= this.sizeD) {
          this.swap(this.mapD[value], this.sizeD);
          this.sizeD += 1;
        }
      } else {
        this.sizeD = this.domD.length;
      }
    }
  }

  public restoreRange(value: number): boolean {
    if (this.sizeD + value <= this.domD.length) {
      this.sizeD += value;
    }
    return false;
  }

  public size(): number {
    if (this.temp) {
      return this.tempSizeD;
    }
    return this.sizeD;
  }

  public startTemp(): void {
    this.tempSizeD = this.sizeD;
    this.temp = true;
  }

  public resetTemp(): void {
    this.tempSizeD = this.sizeD;
  }

  public endTemp(): void {
    this.tempSizeD = this.sizeD;
    this.temp = false;
  }

  private swap(i: number, j: number): void {
    if (i >= this.domD.length || j >= this.domD.length || i < 0 || j < 0) {
      throw Error('Swap out of range: domSize: ' + this.domD.length + ' :i: ' + i + ' :j: ' + j);
    }
    const temp = this.domD[i];
    this.domD[i] = this.domD[j];
    this.domD[j] = temp;
    this.mapD[this.domD[j]] = j;
    this.mapD[this.domD[i]] = i;
  }

  private bind(v: number): void {
    if (this.mapD[v] == null) {
      throw Error('Bind::Element not in domD called: ' + v);
    }
    if (this.mapD[v] >= this.sizeD) {
      this.sizeD = 0;
    } else {
      this.swap(this.mapD[v], this.sizeD - 1);
      this.sizeD -= 1;
    }
  }
}

export default SparseSet;
