import BaseDomain from 'domains/BaseDomain';
import Variable from 'Variable';
import SparseGeneric from 'domains/SparseGeneric';

class BinaryTableSparse extends SparseGeneric<[number, number]> {
  public scope: [Variable, Variable];

  private supports: { [key: string]: Array<number> };
  private originalSupports: { [key: string]: Array<number> };

  public constructor(scope: [Variable, Variable]) {
    const domain = new Array<[number, number]>();
    const domain1 = scope[0].domain.current();
    const domain2 = scope[1].domain.current();
    const supports = {
      [scope[0].toString()]: new Array<number>(),
      [scope[1].toString()]: new Array<number>(),
    };
    for (let j = 0; j < domain2.length; j++) {
      supports[scope[0].toString()][j] = 0;
    }
    for (let i = 0; i < domain1.length; i++) {
      supports[scope[0].toString()][i] = 0;
      for (let j = 0; j < domain2.length; j++) {
        if (domain1[i] !== domain2[j]) {
          domain.push([domain1[i], domain2[j]]);
          supports[scope[0].toString()][i]++;
          supports[scope[1].toString()][j]++;
        }
      }
    }

    super(domain);
    this.scope = scope;
    this.supports = supports;
    this.originalSupports = {
      [this.scope[0].toString()]: supports[scope[0].toString()].slice(),
      [this.scope[1].toString()]: supports[scope[1].toString()].slice(),
    };
  }

  public supported(variable: Variable, value: number): boolean {
    return this.supports[variable.toString()][value] > 0;
  }

  public remove(value: [number, number]): boolean {
    const removal = super.remove(value);
    if (removal) {
      this.supports[this.scope[0].toString()][value[0]]--;
      this.supports[this.scope[1].toString()][value[1]]--;
    }
    return removal;
  }

  public restore(value: [number, number] = null): boolean {
    const restoration = super.restore(value);
    if (restoration) {
      if (value != null) {
        this.supports[this.scope[0].toString()][value[0]]++;
        this.supports[this.scope[1].toString()][value[1]]++;
      } else {
        this.supports[this.scope[0].toString()] = this.originalSupports[this.scope[0].toString()];
        this.supports[this.scope[1].toString()] = this.originalSupports[this.scope[1].toString()];
      }
    }
    return restoration;
  }

  public restoreRange(count: number): boolean {
    const restoration = super.restoreRange(count);
    if (restoration) {
      const size = this.size();
      for (let i = size; i < size + count; i++) {
        const tuple = super.get(i);
        this.supports[this.scope[0].toString()][tuple[0]]++;
        this.supports[this.scope[1].toString()][tuple[1]]++;
      }
    }

    return restoration;
  }
}

export default BinaryTableSparse;
