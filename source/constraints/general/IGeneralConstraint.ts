import ValuesRemoved from 'structures/ValuesRemoved';
import Variable from 'Variable';
import ValueGraph from 'constraints/general/util/ValueGraph';

interface IGeneralConstraint {
    scope: Variable[];
    getInconsistent(temp?: boolean): ValuesRemoved;
    diffPropagation(vvpRemoved?: ValuesRemoved): {
        valid: boolean,
        reduction: ValuesRemoved,
        allRemovals: ValuesRemoved,
    };
    remove(variable: Variable, value: number): void;
    removeVvp(toRemove: ValuesRemoved): void;
    restore(variable: Variable, value: number): void;
    restoreVvp(toRestore: ValuesRemoved): void;
    resetPropagation(): void;
    getValueGraph(): ValueGraph;
    toString(): string;
    copyWithNewVariables(newVariables: Variable[]): IGeneralConstraint;
}

export default IGeneralConstraint;
