import ValuesRemoved from 'structures/ValuesRemoved';
import SparseSet from 'domains/SparseSet';
import Variable from 'Variable';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';

describe('GeneralAllDiff', () => {
  let variables: Array<Variable>;
  let generalAllDiff: GeneralAllDiff;

  it('Does not affect original domains and provides all removed values', () => {
    variables = [
      new Variable(new SparseSet([1, 2, 3]), 'b'),
      new Variable(new SparseSet([1, 2]), 'a'),
      new Variable(new SparseSet([1, 2]), 'c'),
      // new Variable(new SparseSet([1, 2, 3, 4]), 'd'),
    ];
    generalAllDiff = new GeneralAllDiff(variables);

    const inconsistent = generalAllDiff.getInconsistent();
    expect(variables[0].domain.current().sort()).toEqual([1, 2, 3]);
    expect(variables[1].domain.current().sort()).toEqual([1, 2]);
    expect(variables[2].domain.current().sort()).toEqual([1, 2]);
    // expect(variables[3].domain.current().sort()).toEqual([1, 2, 3, 4]);

    expect(inconsistent.getRemoved(variables[0])).toEqual(new Set([1, 2]));
    expect(inconsistent.getRemoved(variables[1])).toEqual(undefined);
    expect(inconsistent.getRemoved(variables[2])).toEqual(undefined);
    // expect(inconsistent.getRemoved(variables[3])).toEqual(new Set([1, 2, 3]));
  });

  it('Works correctly incrementally', () => {
    variables = [
      new Variable(new SparseSet([1, 2, 3]), 'a'),
      new Variable(new SparseSet([1, 2, 3]), 'b'),
      new Variable(new SparseSet([1, 2, 3]), 'c'),
    ];
    generalAllDiff = new GeneralAllDiff(variables);

    const initialInconsistent = generalAllDiff.diffPropagation();
    expect(initialInconsistent.valid).toEqual(true);
    expect(initialInconsistent.reduction.isEmpty()).toEqual(true);

    const firstValuesRemoved = new ValuesRemoved({
      [variables[1].toString()]: {
        variable: variables[1],
        removed: new Set([3]),
      },
      [variables[2].toString()]: {
        variable: variables[2],
        removed: new Set([3]),
      },
    });

    const firstInconsistent = generalAllDiff.diffPropagation(firstValuesRemoved);
    expect(firstInconsistent.valid).toEqual(true);
    expect(firstInconsistent.reduction.getRemoved(variables[0])).toEqual(new Set([1, 2]));
    expect(firstInconsistent.reduction.getRemoved(variables[1])).toEqual(undefined);
    expect(firstInconsistent.reduction.getRemoved(variables[2])).toEqual(undefined);
    expect(firstInconsistent.allRemovals.getRemoved(variables[0])).toEqual(new Set([1, 2]));
    expect(firstInconsistent.allRemovals.getRemoved(variables[1])).toEqual(new Set([3]));
    expect(firstInconsistent.allRemovals.getRemoved(variables[2])).toEqual(new Set([3]));

    generalAllDiff.restoreVvp(firstValuesRemoved);
    generalAllDiff.restoreVvp(firstInconsistent.reduction);
    const secondValuesRemoved = new ValuesRemoved({
      [variables[0].toString()]: {
        variable: variables[0],
        removed: new Set([3]),
      },
      [variables[2].toString()]: {
        variable: variables[2],
        removed: new Set([3]),
      },
    });
    const secondInconsistent = generalAllDiff.diffPropagation(secondValuesRemoved);
    expect(secondInconsistent.valid).toEqual(true);
    expect(secondInconsistent.reduction.getRemoved(variables[0])).toEqual(undefined);
    expect(secondInconsistent.reduction.getRemoved(variables[1])).toEqual(new Set([1, 2]));
    expect(secondInconsistent.reduction.getRemoved(variables[2])).toEqual(undefined);
    expect(secondInconsistent.allRemovals.getRemoved(variables[0])).toEqual(new Set([3]));
    expect(secondInconsistent.allRemovals.getRemoved(variables[1])).toEqual(new Set([1, 2]));
    expect(secondInconsistent.allRemovals.getRemoved(variables[2])).toEqual(new Set([3]));
  });
});
