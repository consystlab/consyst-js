import ValuesRemoved from 'structures/ValuesRemoved';
import SparseSet from 'domains/SparseSet';
import Variable from 'Variable';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';
import ValueGraph, {
  VariableVertex,
  DomainVertex,
} from 'constraints/general/util/ValueGraph';

describe('ValueGraph', () => {
  let variables: Array<Variable>;
  let valueGraph: ValueGraph;

  it('Does not return a nil in matching', () => {
    variables = [
      new Variable(new SparseSet([1, 2, 3]), 'a'),
      new Variable(new SparseSet([1, 2, 3]), 'b'),
      new Variable(new SparseSet([1, 2, 3]), 'c'),
    ];
    const generalAllDiff = new GeneralAllDiff(variables);
    valueGraph = new ValueGraph(generalAllDiff);

    const firstMatching = valueGraph.maxMatching();

    valueGraph.removeEdge(variables[0], 3);
    valueGraph.removeEdge(variables[1], 3);
    valueGraph.removeEdge(variables[2], 3);

    const removedEdge = firstMatching.find(({ left, right}) => right.value === 3);
    const leftVertices = [removedEdge.left];
    const rightVertices = [removedEdge.right];
    const modifiedPrevMatching = firstMatching.filter(
      ({ left, right }) => right.value !== 3
    );

    expect(modifiedPrevMatching.length).toEqual(2);
    const expandedMatching = valueGraph.expandMatching(modifiedPrevMatching, leftVertices, rightVertices);
    expect(expandedMatching.length).toEqual(2);
  });
});
