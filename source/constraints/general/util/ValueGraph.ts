import ValuesRemoved from 'structures/ValuesRemoved';
import Queue from 'structures/Queue';
import Variable from 'Variable';
import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';

export default class ValueGraph {
  public edges: IGVEdge[];
  public vertices: Vertex[];

  private leftVertices: VariableVertex[];
  private rightVertices: DomainVertex[];

  constructor(private constraint: IGeneralConstraint) {
    this.leftVertices = new Array<VariableVertex>();
    this.rightVertices = new Array<DomainVertex>();
    this.edges = new Array<IGVEdge>();
    this.vertices = new Array<Vertex>();
    const values: { [key: number]: DomainVertex } = {};
    const overallscope = new Set<number>();
    for (let i = 0; i < constraint.scope.length; i++) {
      const currentDomain = constraint.scope[i].isAssigned() ?
        [constraint.scope[i].getAssignment()]
        : constraint.scope[i].domain.current();
      for (let j = 0; j < currentDomain.length; j++) {
        overallscope.add(currentDomain[j]);
      }
    }
    overallscope.forEach(val => {
      const vertex = new DomainVertex(val);
      this.vertices.push(vertex);
      this.rightVertices.push(vertex);
      values[val] = vertex;
    });
    for (let i = 0; i < constraint.scope.length; i++) {
      const vertex = new VariableVertex(constraint.scope[i]);
      this.vertices.push(vertex);
      this.leftVertices.push(vertex);
      const currentDomain = constraint.scope[i].isAssigned() ?
        [constraint.scope[i].getAssignment()]
        : constraint.scope[i].domain.current();
      for (let j = 0; j < currentDomain.length; j++) {
        const edge = { left: vertex, right: values[currentDomain[j]] };
        this.edges.push(edge);
        edge.left.edges.push(edge);
        edge.left.neighbors.add(edge.right);
        edge.right.edges.push(edge);
        edge.right.neighbors.add(edge.left);
      }
    }
  }

  public getLeftVertices = () => this.leftVertices.slice();
  public getRightVertices = () => this.rightVertices.slice();

  public removeEdge(variable: Variable, removed: number): boolean {
    const removeEdgeIndex = this.edges.findIndex(e => e.left.value === variable && e.right.value === removed);
    if (removeEdgeIndex === -1) {
      return false;
    }
    const edge = this.edges.splice(removeEdgeIndex, 1)[0];
    edge.left.edges.splice(edge.left.edges.findIndex(e => e.right.value === removed), 1);
    edge.left.neighbors.delete(edge.right);
    edge.right.edges.splice(edge.right.edges.findIndex(e => e.left.value === variable), 1);
    edge.right.neighbors.delete(edge.left);
    return true;
  }

  public removeEdgesVvp(valuesRemoved: ValuesRemoved): void {
    valuesRemoved.forEach((variable, removedSet) => {
      removedSet.forEach(removed => {
        this.removeEdge(variable, removed);
      });
    });
  }

  public restoreEdgesVvp(toRestore: ValuesRemoved): void {
    toRestore.forEach((variable, removed) => {
      const variableVertex = this.leftVertices.find(vertex => vertex.value === variable);
      if (variableVertex) {
        removed.forEach(removedValue => {
          const domainVertex = this.rightVertices.find(vertex => vertex.value === removedValue);
          const edge = {
            left: variableVertex,
            right: domainVertex,
          };
          this.edges.push(edge);
          variableVertex.edges.push(edge);
          domainVertex.edges.push(edge);
          variableVertex.neighbors.add(domainVertex);
          domainVertex.neighbors.add(variableVertex);
        });
      }
    });
  }

  public restoreEdge(variable: Variable, value: number): boolean {
    const variableVertex = this.leftVertices.find(vertex => vertex.value === variable);
    if (variableVertex) {
      const preEdge = Array.from(variableVertex.neighbors).find(vertex => vertex.value === value);
      if (preEdge) {
        return false;
      }
      const domainVertex = this.rightVertices.find(vertex => vertex.value === value);
      if (!domainVertex) {
        throw new Error('Domain Vertex not found');
      }
      const edge = {
        left: variableVertex,
        right: domainVertex,
      };
      this.edges.push(edge);
      variableVertex.edges.push(edge);
      domainVertex.edges.push(edge);
      variableVertex.neighbors.add(domainVertex);
      domainVertex.neighbors.add(variableVertex);
      return true;
    }
    return false;
  }

  public maxMatching = () => this.matching([], this.leftVertices, this.rightVertices);
  public expandMatching = (
    prevMatching: Array<{ left: VariableVertex, right: DomainVertex }>,
    leftVertices: Array<VariableVertex>,
    rightVertices: Array<DomainVertex>,
  ) => this.matching(prevMatching, leftVertices, rightVertices)

  public generateDirectedGraph(
    matching: Array<{ left: VariableVertex, right: DomainVertex }>
  ): { dvertices: DVertex[], dedges: IDGVEdge[] } {
    const pairs: { [key: string]: DomainVertex } = {};
    for (let i = 0; i < matching.length; i++) {
      pairs[matching[i].left.id] = matching[i].right;
    }
    const directedVertices = new Array<DVertex>();
    const vertexMap: { [key: string]: DVertex } = {};
    for (let i = 0; i < this.leftVertices.length; i++) {
      const dv = new DVariableVertex(this.leftVertices[i].value);
      directedVertices.push(dv);
      vertexMap[this.leftVertices[i].id] = dv;
    }
    for (let i = 0; i < this.rightVertices.length; i++) {
      const dv = new DDomainVertex(this.rightVertices[i].value);
      directedVertices.push(dv);
      vertexMap[this.rightVertices[i].id] = dv;
    }
    const directedEdges = new Array<IDGVEdge>();
    for (let i = 0; i < this.edges.length; i++) {
      const edge = this.edges[i];
      if (pairs[edge.left.id] === edge.right) {
        directedEdges.push({ source: vertexMap[edge.left.id], term: vertexMap[edge.right.id] });
        vertexMap[edge.left.id].outNeighbors.add(vertexMap[edge.right.id]);
        vertexMap[edge.right.id].inNeighbors.add(vertexMap[edge.left.id]);
      } else {
        directedEdges.push({ source: vertexMap[edge.right.id], term: vertexMap[edge.left.id] });
        vertexMap[edge.right.id].outNeighbors.add(vertexMap[edge.left.id]);
        vertexMap[edge.left.id].inNeighbors.add(vertexMap[edge.right.id]);
      }
    }
    return { dvertices: directedVertices, dedges: directedEdges };
  }

  public stronglyConnectedComponents(dgraph: { dvertices: DVertex[], dedges: IDGVEdge[] }): Array<Set<DVertex>> {
    // https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm
    let index = 0;
    const indices: { [key: string]: number } = {};
    const lowlink: { [key: string]: number } = {};
    const onStack: { [key: string]: boolean } = {};
    const s = new Array<DVertex>();
    const sccs = new Array<Set<DVertex>>();

    const strongConnect = (v: DVertex) => {
      indices[v.id] = index;
      lowlink[v.id] = index;
      index += 1;
      s.push(v);
      onStack[v.id] = true;

      const outIt = v.outNeighbors.values();
      for (let out = outIt.next(); !out.done; out = outIt.next()) {
        if (!indices[out.value.id]) {
          strongConnect(out.value);
          lowlink[v.id] = Math.min(lowlink[v.id], lowlink[out.value.id]);
        } else if (onStack[out.value.id]) {
          lowlink[v.id] = Math.min(lowlink[v.id], lowlink[out.value.id]);
        }
      }

      if (lowlink[v.id] === indices[v.id]) {
        const scc = new Set<DVertex>();
        let w: DVertex;
        do {
          w = s.pop();
          onStack[w.id] = false;
          scc.add(w);
        } while (w !== v);
        sccs.push(scc);
      }
    };

    for (let i = 0; i < dgraph.dvertices.length; i++) {
      if (!indices[dgraph.dvertices[i].id]) {
        strongConnect(dgraph.dvertices[i]);
      }
    }

    return sccs;
  }

  public classifyEdges(
    directedGraph: { dvertices: DVertex[], dedges: IDGVEdge[] },
    sccs: Array<Set<DVertex>>
  ): { bad: IDGVEdge[], vital: IDGVEdge[] } {
    const setMap: { [key: string]: Set<DVertex> } = {};
    const usedEdges = new Set<IDGVEdge>();
    const findPath = (source: DVertex, term: DVertex) => {
      for (let i = 0; i < directedGraph.dedges.length; i++) {
        if (directedGraph.dedges[i].source === source
          && directedGraph.dedges[i].term === term) {
          return directedGraph.dedges[i];
        }
      }
    };

    // Find edges in simple paths starting from free vertices
    const usedPaths = (vertex: DDomainVertex) => {
      const inprogress = new Queue<DVertex>();
      const visited = new Set<DVertex>();
      const parents: { [key: string]: DVertex } = {};
      inprogress.push(vertex);
      parents[vertex.id] = null;
      while (!inprogress.isEmpty()) {
        const v = inprogress.pop();
        v.outNeighbors.forEach(outVertex => {
          if (!inprogress.contains(outVertex) && !visited.has(outVertex)) {
            parents[outVertex.id] = v;
            inprogress.push(outVertex);
          }
        });
        visited.add(v);
      }
      for (let i = 0; i < directedGraph.dvertices.length; i++) {
        if (directedGraph.dvertices[i] instanceof DDomainVertex
          && parents[directedGraph.dvertices[i].id]) {
          // Mark edges as used
          const parent = parents[directedGraph.dvertices[i].id];
          const parentparent = parents[parent.id];
          usedEdges.add(findPath(parent, directedGraph.dvertices[i]));
          usedEdges.add(findPath(parentparent, parent));
        }
      }
    };
    for (let i = 0; i < directedGraph.dvertices.length; i++) {
      if (directedGraph.dvertices[i] instanceof DDomainVertex) {
        if (directedGraph.dvertices[i].inNeighbors.size === 0) {
          usedPaths(directedGraph.dvertices[i] as DDomainVertex);
        }
      }
    }

    // Find cycles in stronglyConnectedComponents
    for (let i = 0; i < sccs.length; i++) {
      sccs[i].forEach(v => {
        setMap[v.id] = sccs[i];
      });
    }
    for (let i = 0; i < directedGraph.dedges.length; i++) {
      if (setMap[directedGraph.dedges[i].source.id] != null
        && setMap[directedGraph.dedges[i].source.id] === setMap[directedGraph.dedges[i].term.id]) {
        // Then inside component
        usedEdges.add(directedGraph.dedges[i]);
      }
    }
    for (let i = 0; i < directedGraph.dedges.length; i++) {
      if (setMap[directedGraph.dedges[i].source.id] != null
        && setMap[directedGraph.dedges[i].source.id] === setMap[directedGraph.dedges[i].term.id]) {
        // Then inside component
        usedEdges.add(directedGraph.dedges[i]);
      }
    }

    const removedEdges = new Array<IDGVEdge>();
    const vitalEdges = new Array<IDGVEdge>();
    for (let i = 0; i < directedGraph.dedges.length; i++) {
      if (!usedEdges.has(directedGraph.dedges[i])) {
        // Edge is either vital or removeable
        if (directedGraph.dedges[i].source instanceof DVariableVertex) {
          // Vital Edge
          vitalEdges.push(directedGraph.dedges[i]);
        } else {
          // Removeable Edge
          removedEdges.push(directedGraph.dedges[i]);
        }
      }
    }
    return {
      bad: removedEdges,
      vital: vitalEdges,
    };
  }

  public convertEdges(badEdges: IDGVEdge[]): ValuesRemoved {
    const removed = new ValuesRemoved();
    for (let i = 0; i < badEdges.length; i++) {
      let variableVertex: DVariableVertex;
      let domainVertex: DDomainVertex;
      if (badEdges[i].source instanceof DVariableVertex) {
        variableVertex = badEdges[i].source as DVariableVertex;
        domainVertex = badEdges[i].term as DDomainVertex;
      } else {
        variableVertex = badEdges[i].term as DVariableVertex;
        domainVertex = badEdges[i].source as DDomainVertex;
      }
      removed.initialize(variableVertex.value);
      removed.addRemoved(variableVertex.value, domainVertex.value);
    }
    return removed;
  }

  private matching(
    prevMatching: Array<{ left: VariableVertex, right: DomainVertex}>,
    newLeftVertices: Array<VariableVertex>,
    newRightVertices: Array<DomainVertex>
  ): Array<{ left: VariableVertex, right: DomainVertex }> {
    // https://en.wikipedia.org/wiki/Hopcroft%E2%80%93Karp_algorithm#Pseudocode
    const nil: Vertex = new Vertex('null');
    const pairu: { [key: string]: DomainVertex } = {};
    const pairv: { [key: string]: VariableVertex } = {};
    const dist: { [key: string]: number } = {};

    const leftVertices = newLeftVertices.slice();
    const rightVertices = newRightVertices.slice();

    prevMatching.forEach(edge => {
      leftVertices.push(edge.left);
      rightVertices.push(edge.right);
      pairu[edge.left.id] = edge.right;
      pairv[edge.right.id] = edge.left;
    });
    for (let i = 0; i < newLeftVertices.length; i++) {
      pairu[newLeftVertices[i].id] = nil;
    }
    for (let i = 0; i < newRightVertices.length; i++) {
      pairv[newRightVertices[i].id] = nil;
    }

    const bfs = () => {
      const queue = new Queue<VariableVertex>();
      for (let i = 0; i < leftVertices.length; i++) {
        if (pairu[leftVertices[i].id] === nil) {
          dist[leftVertices[i].id] = 0;
          queue.push(leftVertices[i]);
        } else {
          dist[leftVertices[i].id] = Number.POSITIVE_INFINITY;
        }
      }
      dist[nil.id] = Number.POSITIVE_INFINITY;
      while (!queue.isEmpty()) {
        const u = queue.pop();
        if (dist[u.id] < dist[nil.id]) {
          u.neighbors.forEach(v => {
            if (dist[pairv[v.id].id] === Number.POSITIVE_INFINITY) {
              dist[pairv[v.id].id] = dist[u.id] + 1;
              queue.push(pairv[v.id]);
            }
          });
        }
      }
      return dist[nil.id] !== Number.POSITIVE_INFINITY;
    };

    const dfs = (u: VariableVertex) => {
      if (u !== nil) {
        const neighborIterator = u.neighbors.values();
        for (let neighbor = neighborIterator.next(); !neighbor.done; neighbor = neighborIterator.next()) {
          const v = neighbor.value;
          if (dist[pairv[v.id].id] === dist[u.id] + 1) {
            if (dfs(pairv[v.id]) === true) {
              pairv[v.id] = u;
              pairu[u.id] = v;
              return true;
            }
          }
        }
        dist[u.id] = Number.POSITIVE_INFINITY;
        return false;
      }
      return true;
    };

    let matching = 0;
    while (bfs() === true) {
      for (let i = 0; i < leftVertices.length; i++) {
        if (pairu[leftVertices[i].id] === nil) {
          if (dfs(leftVertices[i]) === true) {
            matching += 1;
          }
        }
      }
    }
    const pairs = new Array<{ 'left': VariableVertex, 'right': DomainVertex }>();
    Object.values(leftVertices).forEach(v => {
      if (pairu[v.id] !== nil) {
        pairs.push({ left: v, right: pairu[v.id] });
      }
    });
    return pairs;
  }
}

export interface IGVEdge {
  left: Vertex;
  right: Vertex;
}

export interface IDGVEdge {
  source: DVertex;
  term: DVertex;
}

export class Vertex {
  public edges: IGVEdge[];
  public value: any;
  public neighbors: Set<Vertex>;
  public id: string;

  constructor(value: any) {
    this.edges = new Array<IGVEdge>();
    this.id = 'vertex_' + value.toString();
    this.neighbors = new Set<Vertex>();
  }
}

export class DVertex extends Vertex {
  public inNeighbors: Set<DVertex>;
  public outNeighbors: Set<DVertex>;

  constructor(value: any) {
    super(value);
    this.inNeighbors = new Set<DVertex>();
    this.outNeighbors = new Set<DVertex>();
  }
}

export class VariableVertex extends Vertex {
  public edges: IGVEdge[];

  constructor(public value: Variable) {
    super(value);
  }
}

export class DVariableVertex extends DVertex {
  public dedges: IDGVEdge[];

  constructor(public value: Variable) {
    super(value);
  }
}

export class DomainVertex extends Vertex {
  public edges: IGVEdge[];

  constructor(public value: number) {
    super('d' + value);
  }
}

export class DDomainVertex extends DVertex {
  public dedges: IDGVEdge[];

  constructor(public value: number) {
    super('d' + value);
  }
}
