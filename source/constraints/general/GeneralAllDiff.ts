import ValuesRemoved from 'structures/ValuesRemoved';
import Variable from 'Variable';
import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import ValueGraph, {
  VariableVertex,
  DomainVertex,
  DVertex,
  IDGVEdge,
} from 'constraints/general/util/ValueGraph';

export default class GeneralAllDiff implements IGeneralConstraint {
  private id: string;
  private valueGraph: ValueGraph;
  private vitalEdges: Array<IDGVEdge>;
  private propMatching: Array<{ left: VariableVertex, right: DomainVertex }>;
  private propDirectedGraph: { dvertices: DVertex[], dedges: IDGVEdge[] };

  constructor(public scope: Variable[]) {
    this.id = 'gad-[';
    const check: { [key: string]: Variable } = {};
    const variableIds = this.scope.map(variable => {
      if (check[variable.toString()] != null) {
        throw new TypeError('GeneralConstraints::GeneralAllDiff: Duplicate variable in scope.');
      }
      check[variable.toString()] = variable;
      return variable.toString();
    });
    this.id += variableIds.join(',');
    this.id += ']';
    for (const variable of this.scope) {
      variable.addGeneralConstraint(this);
    }
  }

  public getInconsistent(): ValuesRemoved {
    const valueGraph = new ValueGraph(this);
    const matching = valueGraph.maxMatching();
    const directedGraph = valueGraph.generateDirectedGraph(matching);
    const stronglyConnectedComponents = valueGraph.stronglyConnectedComponents(directedGraph);
    const classifications = valueGraph.classifyEdges(directedGraph, stronglyConnectedComponents);
    const removed = valueGraph.convertEdges(classifications.bad);
    removed.setConstraintVariables(new Set<Variable>(this.scope));
    return removed;
  }

  public diffPropagation(vvpRemoved: ValuesRemoved = new ValuesRemoved()): {
    valid: boolean,
    reduction: ValuesRemoved,
    allRemovals: ValuesRemoved,
  } {
    const edgesRemoved = new Array<{ left: VariableVertex, right: DomainVertex }>();
    // Track the actual changes so that we can undo them later
    const reduction = new ValuesRemoved();
    const allRemovals = new ValuesRemoved();
    let valid = true;
    if (this.valueGraph) {
      const oldMatching = this.propMatching.slice();
      let vitalRemoved = false;
      this.vitalEdges.forEach(edge => {
        vvpRemoved.forEach((variable, removedValues) => {
          if (edge.source.value === variable && removedValues.has(edge.term.value)) {
            vitalRemoved = true;
          }
        });
      });
      if (vitalRemoved) {
        valid = false;
      }

      let needToComputeMatching = (oldMatching.length !== this.scope.length);
      vvpRemoved.forEach((variable, removedValues) => {
        removedValues.forEach(removedValue => {
          const removeIndex = this.propMatching.findIndex(
            x => x.left.value === variable && x.right.value === removedValue
          );
          if (removeIndex > -1) {
            const removalPair = this.propMatching[removeIndex];
            this.propMatching.splice(removeIndex, 1);
            needToComputeMatching = true;
          }
          this.valueGraph.removeEdge(variable, removedValue);
          allRemovals.addRemoved(variable, removedValue);
        });
      });

      if (needToComputeMatching) {
        const leftVertices = this.valueGraph.getLeftVertices()
          .filter(vertex => !this.propMatching.find(edge => edge.left === vertex));
        const rightVertices = this.valueGraph.getRightVertices()
          .filter(vertex => !this.propMatching.find(edge => edge.right === vertex));
        this.propMatching = this.valueGraph.expandMatching(this.propMatching, leftVertices, rightVertices);
      }
    } else {
      this.valueGraph = new ValueGraph(this);
      this.propMatching = this.valueGraph.maxMatching();
    }
    this.propDirectedGraph = this.valueGraph.generateDirectedGraph(this.propMatching);
    const stronglyConnectedComponents = this.valueGraph.stronglyConnectedComponents(this.propDirectedGraph);
    const { bad, vital } = this.valueGraph.classifyEdges(this.propDirectedGraph, stronglyConnectedComponents);
    this.vitalEdges = vital;
    const removed = this.valueGraph.convertEdges(bad);
    removed.forEach((variable, removedSet) => {
      reduction.addAllRemoved(variable, removedSet);
      allRemovals.addAllRemoved(variable, removedSet);
    });
    this.valueGraph.removeEdgesVvp(removed);
    reduction.setConstraintVariables(new Set<Variable>(this.scope));
    if (!valid) {
      this.valueGraph = null;
      this.vitalEdges = [];
    }
    return { valid, reduction, allRemovals };
  }

  public removeVvp(toRemove: ValuesRemoved) {
    if (this.valueGraph) {
      this.valueGraph.removeEdgesVvp(toRemove);
    }
  }

  public restoreVvp(toRestore: ValuesRemoved) {
    if (this.valueGraph) {
      this.valueGraph.restoreEdgesVvp(toRestore);
      // Need to recalculate these
      this.vitalEdges = [];
    }
  }

  public remove(variable: Variable, value: number) {
    if (this.valueGraph) {
      const index = this.propMatching.findIndex(edge => edge.left.value === variable && edge.right.value === value);
      if (index > -1) {
        this.propMatching.splice(index, 1);
      }
      this.valueGraph.removeEdge(variable, value);
    }
  }

  public restore(variable: Variable, value: number): void {
    if (this.valueGraph) {
      this.vitalEdges = [];
      this.valueGraph.restoreEdge(variable, value);
    }
  }

  public resetPropagation() {
    this.vitalEdges = null;
    this.valueGraph = null;
  }

  public getValueGraph(): ValueGraph {
    return this.valueGraph;
  }

  public getMatching(): Array<{ left: VariableVertex, right: DomainVertex }> {
    return this.propMatching;
  }

  public toString(): string {
    return this.id;
  }

  public copyWithNewVariables(newVariables: Variable[]): GeneralAllDiff {
    const ids = this.scope.map(variable => variable.id);
    return new GeneralAllDiff(newVariables.filter(variable => ids.includes(variable.id)));
  }
}
