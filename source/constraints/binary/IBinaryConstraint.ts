import Variable from 'Variable';
import VariableValuePairs from 'structures/VariableValuePairs';

interface IBinaryConstraint {
    scope: Variable[];

    check(vvps: VariableValuePairs): boolean;

    copyWithNewVariables(newVariables: Variable[]): IBinaryConstraint;
    toString(): string;
}

export default IBinaryConstraint;
