import SparseSet from 'domains/SparseSet';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import BinaryAllDiff from 'constraints/binary/BinaryAllDiff';
import VariableValuePairs from 'structures/VariableValuePairs';
import VariableValuePair from 'structures/VariableValuePair';

describe('BinaryAllDiff', () => {
  let variables: Array<Variable>;
  let constraint: BinaryAllDiff;

  it('Completes checks correctly', () => {
    variables = [
      new Variable(new SparseSet([1, 2, 3]), 'a'),
      new Variable(new SparseSet([1, 2]), 'b'),
    ];
    constraint = new BinaryAllDiff([variables[0], variables[1]]);

    variables[0].domain.current().forEach(d0 => {
      variables[1].domain.current().forEach(d1 => {
        const vvps = new VariableValuePairs([
          {
            key: variables[0],
            value: d0,
          },
          {
            key: variables[1],
            value: d1,
          },
        ]);
        const check = constraint.check(vvps);
        expect(check).toEqual(d0 !== d1);
      });
    });
  });
});
