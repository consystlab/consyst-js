import Variable from 'Variable';
import VariableValuePairs from 'structures/VariableValuePairs';
import IBinaryConstraint from 'constraints/binary/IBinaryConstraint';
import SearchAnalytics from 'structures/SearchAnalytics';

const searchAnalytics = SearchAnalytics.getInstance();

export default class BinaryAllDiff implements IBinaryConstraint {
  public id: string;

  constructor(public scope: [Variable, Variable]) {
    scope.forEach(variable => {
      variable.addBinaryConstraint(this);
    });
    this.id = 'bad-[';
    this.id += scope.map(variable => variable.toString()).join(',');
    this.id += ']';
  }

  public check(vvps: VariableValuePairs): boolean {
    searchAnalytics.constraintChecks++;
    if (vvps.size() !== 2) {
      return false;
    }
    const keys = vvps.keys();
    for (let i = 0; i < keys.length; i++) {
      if (!(this.scope.indexOf(keys[i]) > -1)) {
        return false;
      }
    }
    const values = vvps.values();
    return values[0] !== values[1];
  }

  public copyWithNewVariables(newVariables: Variable[]): BinaryAllDiff {
    const ids = this.scope.map(variable => variable.id);
    return new BinaryAllDiff(newVariables.filter(variable => ids.includes(variable.id)) as [Variable, Variable]);
  }

  public toString(): string {
    return this.id;
  }
}
