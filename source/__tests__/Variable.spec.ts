import SparseSet from 'domains/SparseSet';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import BinaryAllDiff from 'constraints/binary/BinaryAllDiff';
import VariableValuePairs from 'structures/VariableValuePairs';
import VariableValuePair from 'structures/VariableValuePair';

describe('Variable', () => {
  it('Takes initialization well', () => {
    const domain = new SparseSet([1, 2, 3]);
    const variable = new Variable(domain, 'a');
    expect(variable.id).toEqual('a');
    expect(variable.domain).toBe(domain);
  });

  it('Binary Revise works', () => {
    const variable1 = new Variable(new SparseSet([1, 2, 3]), 'a');
    const variable2 = new Variable(new SparseSet([1, 2]), 'b');
    const constraint = new BinaryAllDiff([variable1, variable2]);

    const revision1 = variable1.binaryRevise(variable2);
    expect(revision1).toEqual(false);
    const revision2 = variable2.binaryRevise(variable1);
    expect(revision2).toEqual(false);

    variable2.domain.remove(2);
    const revision3 = variable1.binaryRevise(variable2);
    expect(revision3).toEqual(true);
    expect(variable1.domain.current().sort()).toEqual([2, 3]);
    const revision4 = variable2.binaryRevise(variable1);
    expect(revision4).toEqual(false);
  });

  it('preserves assignment on copy', () => {
    const variable1 = new Variable(new SparseSet([1, 2, 3]), 'asdf');
    variable1.domain.remove(2);
    variable1.assign(3);

    const variable2 = Variable.copy(variable1);
    expect(variable2.isAssigned()).toEqual(true);
    expect(variable2.getAssignment()).toEqual(3);

    variable2.unassign();
    expect(variable2.domain.current().sort()).toEqual([1, 3]);
  });
});
