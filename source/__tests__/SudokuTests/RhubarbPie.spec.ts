import ValuesRemoved from 'structures/ValuesRemoved';
import SparseSet from 'domains/SparseSet';
import Variable from 'Variable';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';

import TestUtils from 'util/sudokuTestUtils';

const PUZZLE_STRING = '000020304080350000900004026000500060040000100700002009500407010009000080000103670';

describe('Rhubarb Pie', () => {
  it('is solved by AC', () => {
    const sudoku = TestUtils.sudokuLoader(PUZZLE_STRING);
    sudoku.binaryArcConsistency();

    sudoku.variables.forEach(variable => {
      expect(variable.domain.size()).toEqual(1);
    });
  });
});
