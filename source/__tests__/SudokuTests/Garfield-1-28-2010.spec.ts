import ValuesRemoved from 'structures/ValuesRemoved';
import SparseSet from 'domains/SparseSet';
import Variable from 'Variable';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';

import TestUtils from 'util/sudokuTestUtils';

const PUZZLE_STRING = '502080000000209070300700000000600041800020005210007000000003002060402000000050903';

describe('Garfield', () => {
  it('is not inconsistent', () => {
    const sudoku = TestUtils.sudokuLoader(PUZZLE_STRING);
    sudoku.generalArcConsistency();

    sudoku.variables.forEach(variable => {
      expect(variable.domain.size()).not.toEqual(0);
    });
  });
  it('is solved by GAC', () => {
    const sudoku = TestUtils.sudokuLoader(PUZZLE_STRING);
    sudoku.generalArcConsistency();

    sudoku.variables.forEach(variable => {
      expect(variable.domain.size()).toEqual(1);
    });
  });
});
