import ValuesRemoved from 'structures/ValuesRemoved';
import SparseSet from 'domains/SparseSet';
import Variable from 'Variable';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';
import Sudoku from 'Sudoku';
import BCSSP from 'searches/bcssp';
import DomWDegOrdering from 'orderings/variable/DomWDegOrdering';
import FCGAC from 'searches/fcGac';
import { SearchResults } from 'Enumerations';
import FC from 'searches/fc';
import LexicoSingletonOrdering from 'orderings/variable/LexicoSingletonOrdering';

import TestUtils from 'util/sudokuTestUtils';

const PUZZLE_STRING = '100000060000100003005002900009001000700040080030500002500400006008060070070005000';

describe('AI Honeypot', () => {
  let sudoku: Sudoku;

  beforeEach(() => {
    sudoku = TestUtils.sudokuLoader(PUZZLE_STRING);
  });
  it('is not solved by AC', () => {
    sudoku.binaryArcConsistency();

    const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
    expect(solved).toBe(false);
  });

  it('is not solved by GAC', () => {
    sudoku.generalArcConsistency();

    const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
    expect(solved).toBe(false);
  });

  it('is not solved by SAC', () => {
    sudoku.singletonArcConsistency();

    const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
    expect(solved).toBe(false);
  });

  it('is solved by SGAC', () => {
    sudoku.singletonGeneralArcConsistency();

    const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
    expect(solved).toBe(true);
  });

  it('is solvable', () => {
    const fcResult = BCSSP(
      sudoku,
      FC,
      LexicoSingletonOrdering,
      true,
      1,
      true
    );
    const solutionResult = BCSSP(
      sudoku,
      FCGAC,
      DomWDegOrdering,
      true,
      1,
      true
    );

    expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
    expect(solutionResult[1].length).toEqual(1);
    sudoku.variables.forEach(variable => {
      expect(variable.domain.contains(solutionResult[1][0][variable.id][1])).toEqual(true);
    });
    expect(solutionResult[2].nodesVisited).toEqual(137);
    expect(solutionResult[2].constraintChecks).toEqual(0);
  });
});
