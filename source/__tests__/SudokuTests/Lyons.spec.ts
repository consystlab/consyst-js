import ValuesRemoved from 'structures/ValuesRemoved';
import SparseSet from 'domains/SparseSet';
import Variable from 'Variable';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';
import Sudoku from 'Sudoku';
import BCSSP from 'searches/bcssp';
import DomWDegOrdering from 'orderings/variable/DomWDegOrdering';
import LexicoSingletonOrdering from 'orderings/variable/LexicoSingletonOrdering';
import FCGAC from 'searches/fcGac';
import FC from 'searches/fc';
import FCRFL from 'searches/fcRfl';
import { SearchResults } from 'Enumerations';

import TestUtils from 'util/sudokuTestUtils';

const PUZZLE_STRING = '000000000000003085001020000000507000004000100090000000500000073002010000000040009';

describe('Lyons 1', () => {
  let sudoku: Sudoku;

  beforeEach(() => {
    sudoku = TestUtils.sudokuLoader(PUZZLE_STRING);
  });

  it('is not inconsistent', () => {
    sudoku.generalArcConsistency();

    sudoku.variables.forEach(variable => {
      expect(variable.domain.size()).not.toEqual(0);
    });
  });

  it('is solved by GAC', () => {
    sudoku.generalArcConsistency();

    sudoku.variables.forEach(variable => {
      expect(variable.domain.size()).toEqual(1);
    });
  });

  // To run this test, make sure you have a minute. It takes a while just using FC.

  // it('fc solves it', () => {
  //   const solutionResult = BCSSP(
  //     sudoku,
  //     FC,
  //     LexicoSingletonOrdering,
  //     true,
  //     1,
  //     true,
  //   );

  //   expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
  //   expect(solutionResult[1].length).toEqual(1);
  //   sudoku.variables.forEach(variable => {
  //     expect(variable.domain.contains(solutionResult[1][0][variable.id][1])).toEqual(true);
  //   });
  //   expect(solutionResult[2].backtracks).toEqual(0);
  //   expect(solutionResult[2].nodesVisited).toEqual(81);
  // });

  // This one also takes quite some time. Only run if you really need to.

  // it('fc-rfl solves it', () => {
  //   const solutionResult = BCSSP(
  //     sudoku,
  //     FCRFL,
  //     DomWDegOrdering,
  //     true,
  //     1,
  //     true,
  //   );

  //   expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
  //   expect(solutionResult[1].length).toEqual(1);
  //   sudoku.variables.forEach(variable => {
  //     expect(variable.domain.contains(solutionResult[1][0][variable.id][1])).toEqual(true);
  //   });
  // });

  it('fcgac solves it', () => {
    const solutionResult = BCSSP(
      sudoku,
      FCGAC,
      DomWDegOrdering,
      true,
      1,
      true,
    );

    expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
    expect(solutionResult[1].length).toEqual(1);
    sudoku.variables.forEach(variable => {
      expect(variable.domain.contains(solutionResult[1][0][variable.id][1])).toEqual(true);
    });
    expect(solutionResult[2].backtracks).toEqual(0);
    expect(solutionResult[2].nodesVisited).toEqual(81);
    expect(solutionResult[2].constraintChecks).toEqual(0);
  });
});
