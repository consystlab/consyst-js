import ValuesRemoved from 'structures/ValuesRemoved';
import SparseSet from 'domains/SparseSet';
import Variable from 'Variable';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';
import Sudoku from 'Sudoku';
import BCSSP from 'searches/bcssp';
import DomWDegOrdering from 'orderings/variable/DomWDegOrdering';
import LexicoSingletonOrdering from 'orderings/variable/LexicoSingletonOrdering';
import FCGAC from 'searches/fcGac';
import FC from 'searches/fc';
import FCRFL from 'searches/fcRfl';
import { SearchResults } from 'Enumerations';

import TestUtils from 'util/sudokuTestUtils';

const PUZZLE_STRING = '000906000000100056020030700600400087100000003790005001009010040380009000000703000';

describe('MensaLevel-4-105', () => {
  let sudoku: Sudoku;

  beforeEach(() => {
    sudoku = TestUtils.sudokuLoader(PUZZLE_STRING);
  });
  it('is not solved by AC', () => {
    sudoku.binaryArcConsistency();

    const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
    expect(solved).toBe(false);
  });

  it('is not solved by GAC', () => {
    sudoku.generalArcConsistency();

    const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
    expect(solved).toBe(false);
  });

  it('is solved by SAC', () => {
    sudoku.singletonArcConsistency();

    sudoku.variables.forEach(variable => {
      expect(variable.domain.size()).toEqual(1);
    });
  });

  it('is solved by SGAC', () => {
    sudoku.singletonGeneralArcConsistency();

    sudoku.variables.forEach(variable => {
      expect(variable.domain.size()).toEqual(1);
    });
  });

  it('fc-rfl solves it', () => {
    const solutionResult = BCSSP(
      sudoku,
      FCRFL,
      LexicoSingletonOrdering,
      true,
      1,
      true,
    );

    expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
    expect(solutionResult[1].length).toEqual(1);
    sudoku.variables.forEach(variable => {
      expect(variable.domain.contains(solutionResult[1][0][variable.id][1])).toEqual(true);
    });
  });
});
