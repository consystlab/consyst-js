import ValuesRemoved from 'structures/ValuesRemoved';
import SparseSet from 'domains/SparseSet';
import Variable from 'Variable';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';
import Sudoku from 'Sudoku';
import BCSSP from 'searches/bcssp';
import DomWDegOrdering from 'orderings/variable/DomWDegOrdering';
import FCGAC from 'searches/fcGac';
import { SearchResults } from 'Enumerations';

import TestUtils from 'util/sudokuTestUtils';

const PUZZLE_STRING = '000000204800070000000000600700200000000106000030000080016000000000050030402000000';

describe('17-givens-1000', () => {
    let sudoku: Sudoku;

    beforeEach(() => {
        sudoku = TestUtils.sudokuLoader(PUZZLE_STRING);
    });
    it('is not solved by AC', () => {
        sudoku.binaryArcConsistency();

        const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
        expect(solved).toBe(false);
    });

    it('is not solved by GAC', () => {
        const consistencyResult = sudoku.generalArcConsistency();

        const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
        expect(solved).toBe(false);
        expect(consistencyResult.consistent).toEqual(true);
    });

    it('is not solved by SAC', () => {
        sudoku.singletonArcConsistency();

        const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
        expect(solved).toBe(false);
    });

    it('is solved by SGAC', () => {
        sudoku.singletonGeneralArcConsistency();

        const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
        expect(solved).toBe(true);
    });

    it('is solvable', () => {
        const solutionResult = BCSSP(
            sudoku,
            FCGAC,
            DomWDegOrdering,
            false,
            50,
            false
        );

        expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
        expect(solutionResult[1].length).toEqual(1);
        sudoku.variables.forEach(variable => {
            expect(variable.domain.contains(solutionResult[1][0][variable.id][1])).toEqual(true);
        });
        expect(solutionResult[2].constraintChecks).toEqual(0);
        expect(solutionResult[2].nodesVisited).toBeGreaterThan(81);
    });

    it('is solvable on a clean slate', () => {
        const solutionResult = BCSSP(
            sudoku,
            FCGAC,
            DomWDegOrdering,
            false,
            50,
            true
        );

        expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
        expect(solutionResult[1].length).toEqual(1);
        sudoku.variables.forEach(variable => {
            expect(variable.domain.contains(solutionResult[1][0][variable.id][1])).toEqual(true);
        });
        expect(solutionResult[2].constraintChecks).toEqual(0);
    });
});
