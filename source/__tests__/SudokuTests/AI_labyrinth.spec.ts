import ValuesRemoved from 'structures/ValuesRemoved';
import SparseSet from 'domains/SparseSet';
import Variable from 'Variable';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';

import TestUtils from 'util/sudokuTestUtils';

const PUZZLE_STRING = '100400800040030009009006050050300000000001600000070002004010900700800004020004080';

describe('AI Labyrinth', () => {
  it('is not solved by AC', () => {
    const sudoku = TestUtils.sudokuLoader(PUZZLE_STRING);
    sudoku.binaryArcConsistency();

    const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
    expect(solved).toBe(false);
  });

  it('is not solved by GAC', () => {
    const sudoku = TestUtils.sudokuLoader(PUZZLE_STRING);
    sudoku.generalArcConsistency();

    const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
    expect(solved).toBe(false);
  });

  it('is not solved by SAC', () => {
    const sudoku = TestUtils.sudokuLoader(PUZZLE_STRING);
    sudoku.singletonArcConsistency();

    const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
    expect(solved).toBe(false);
  });

  it('is solved by SGAC', () => {
    const sudoku = TestUtils.sudokuLoader(PUZZLE_STRING);
    sudoku.singletonGeneralArcConsistency();

    sudoku.variables.forEach(variable => {
      expect(variable.domain.size()).toEqual(1);
    });
  });
});
