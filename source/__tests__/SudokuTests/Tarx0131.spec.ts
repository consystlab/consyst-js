import ValuesRemoved from 'structures/ValuesRemoved';
import SparseSet from 'domains/SparseSet';
import Variable from 'Variable';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';
import Sudoku from 'Sudoku';
import BCSSP from 'searches/bcssp';
import DomWDegOrdering from 'orderings/variable/DomWDegOrdering';
import LexicoSingletonOrdering from 'orderings/variable/LexicoSingletonOrdering';
import FCGAC from 'searches/fcGac';
import FC from 'searches/fc';
import FCRFL from 'searches/fcRfl';
import { SearchResults } from 'Enumerations';

import TestUtils from 'util/sudokuTestUtils';

const PUZZLE_STRING = '000006000000508000008090500000070004007600200010020030002000600040060090390000001';

describe('HardestSudokusThread; tarx0131', () => {
  let sudoku: Sudoku;

  beforeEach(() => {
    sudoku = TestUtils.sudokuLoader(PUZZLE_STRING);
  });

  it('gac-rfl solves it', () => {
    const solutionResult = BCSSP(
      sudoku,
      FCGAC,
      DomWDegOrdering,
      false,
      50,
      true,
    );

    expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
    expect(solutionResult[1].length).toEqual(1);
    sudoku.variables.forEach(variable => {
      expect(variable.domain.contains(solutionResult[1][0][variable.id][1])).toEqual(true);
    });
  });
});
