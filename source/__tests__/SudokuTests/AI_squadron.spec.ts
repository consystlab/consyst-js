import ValuesRemoved from 'structures/ValuesRemoved';
import SparseSet from 'domains/SparseSet';
import Variable from 'Variable';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';
import Sudoku from 'Sudoku';
import BCSSP from 'searches/bcssp';
import DomWDegOrdering from 'orderings/variable/DomWDegOrdering';
import FCGAC from 'searches/fcGac';
import { SearchResults } from 'Enumerations';
import FC from 'searches/fc';
import LexicoSingletonOrdering from 'orderings/variable/LexicoSingletonOrdering';

import TestUtils from 'util/sudokuTestUtils';

const PUZZLE_STRING = '600000200090001005008030040000002001500600900007090000070003002000400500006070080';

describe('AI Squadron', () => {
  let sudoku: Sudoku;

  beforeEach(() => {
    sudoku = TestUtils.sudokuLoader(PUZZLE_STRING);
  });

  it('is solvable', () => {
    const solutionResult = BCSSP(
      sudoku,
      FCGAC,
      DomWDegOrdering,
      true,
      1,
      true
    );

    expect(solutionResult[0]).toEqual(SearchResults.SOLUTIONS);
    expect(solutionResult[1].length).toEqual(1);
    sudoku.variables.forEach(variable => {
      expect(variable.domain.contains(solutionResult[1][0][variable.id][1])).toEqual(true);
    });
    expect(solutionResult[2].constraintChecks).toEqual(0);
  });
});
