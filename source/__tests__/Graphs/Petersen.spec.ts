import ValuesRemoved from 'structures/ValuesRemoved';
import SparseSet from 'domains/SparseSet';
import Variable from 'Variable';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';
import Sudoku from 'Sudoku';

describe('Rhubarb Pie', () => {
  let csp: Sudoku;
  let variables: Array<Variable>;
  let constraints: Array<GeneralAllDiff>;

  beforeEach(() => {
    variables = [
      new Variable(new SparseSet([2, 3]), 'pet_12'),
      new Variable(new SparseSet([1, 2]), 'pet_13'),
      new Variable(new SparseSet([1, 2, 3]), 'pet_14'),
      new Variable(new SparseSet([1, 2, 3]), 'pet_15'),
      new Variable(new SparseSet([1, 2, 3]), 'pet_23'),
      new Variable(new SparseSet([1, 2, 3]), 'pet_24'),
      new Variable(new SparseSet([1, 2, 3]), 'pet_25'),
      new Variable(new SparseSet([1, 2, 3]), 'pet_34'),
      new Variable(new SparseSet([1, 2, 3]), 'pet_35'),
      new Variable(new SparseSet([1, 2, 3]), 'pet_45'),
    ];
    constraints = [
      new GeneralAllDiff([variables[0], variables[7]]),
      new GeneralAllDiff([variables[0], variables[8]]),
      new GeneralAllDiff([variables[0], variables[9]]),

      new GeneralAllDiff([variables[1], variables[5]]),
      new GeneralAllDiff([variables[1], variables[6]]),
      new GeneralAllDiff([variables[1], variables[9]]),

      new GeneralAllDiff([variables[2], variables[4]]),
      new GeneralAllDiff([variables[2], variables[6]]),
      new GeneralAllDiff([variables[2], variables[8]]),

      new GeneralAllDiff([variables[3], variables[4]]),
      new GeneralAllDiff([variables[3], variables[5]]),
      new GeneralAllDiff([variables[3], variables[7]]),

      new GeneralAllDiff([variables[4], variables[9]]),
      new GeneralAllDiff([variables[5], variables[8]]),
      new GeneralAllDiff([variables[6], variables[7]]),
    ];
    csp = new Sudoku(variables, [], constraints);
  });

  it('ssgac does something', () => {
    const removals = csp.ssgac();
  });
});
