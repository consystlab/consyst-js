import IBinaryConstraint from 'constraints/binary/IBinaryConstraint';
import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import Variable from 'Variable';

import verifyConsistencies from 'consistencies/verifyConsistencies';
import BinaryArcConsistency from 'consistencies/descriptive/binaryarcconsistency';
import BinaryForwardChecking from 'consistencies/descriptive/binaryforwardchecking';
import GeneralArcConsistency from 'consistencies/descriptive/generalarcconsistency';
import GeneralForwardChecking from 'consistencies/descriptive/generalforwardchecking';
import SingletonArcConsistency from 'consistencies/descriptive/singletonarcconsistency';
import SingletonGeneralArcConsistency from 'consistencies/descriptive/singletongeneralarcconsistency';
import BiSAC from 'consistencies/descriptive/BiSAC';
import BiSGAC from 'consistencies/descriptive/BiSGAC';
import POAC from 'consistencies/descriptive/POAC';
import POGAC from 'consistencies/descriptive/POGAC';
import SSAC from 'consistencies/descriptive/SSAC';
import SSGAC from 'consistencies/descriptive/SSGAC';

export default class Sudoku {

  public static copy(sudoku: Sudoku): Sudoku {
    const newVariables = sudoku.variables.map(v => Variable.copy(v));
    const newBinaryConstraints = sudoku.binaryConstraints.map(bc => bc.copyWithNewVariables(newVariables));
    const newGeneralConstraints = sudoku.generalConstraints.map(gc => gc.copyWithNewVariables(newVariables));

    return new Sudoku(
      newVariables,
      newBinaryConstraints,
      newGeneralConstraints
    );
  }

  constructor(
    public variables: Variable[] = Array<Variable>(),
    public binaryConstraints: IBinaryConstraint[] = Array<IBinaryConstraint>(),
    public generalConstraints: IGeneralConstraint[] = Array<IGeneralConstraint>()
  ) { }

  public copy = () => Sudoku.copy(this);

  public verifyConsistencies = () => verifyConsistencies(this);
  public binaryArcConsistency = (subset?: Variable[]) => BinaryArcConsistency(this, subset);
  public binaryForwardChecking = (set: Variable[]) => BinaryForwardChecking(set);
  public generalArcConsistency = (specific?: IGeneralConstraint) => GeneralArcConsistency(this, specific);
  public generalForwardChecking = (set: IGeneralConstraint[]) => GeneralForwardChecking(set);
  public singletonArcConsistency = (subset?: Variable[]) => SingletonArcConsistency(this, subset);
  public singletonGeneralArcConsistency = (specific?: IGeneralConstraint) =>
    SingletonGeneralArcConsistency(this, specific)
  public bisac = () => BiSAC(this);
  public bisgac = () => BiSGAC(this);
  public poac = () => POAC(this);
  public pogac = () => POGAC(this);
  public ssac = () => SSAC(this);
  public ssgac = () => SSGAC(this);
}
