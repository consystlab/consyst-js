export default class Queue<T> {

    private queue: T[];

    constructor() {
        this.queue = new Array<T>();
    }

    public contains(item: T): boolean {
        return this.queue.indexOf(item) > -1;
    }

    public push(item: T): void {
        this.queue.push(item);
    }

    public pop(): T {
        return this.queue.shift();
    }

    public peek(): T {
        return this.queue[0] || null;
    }

    public isEmpty(): boolean {
        return this.queue.length === 0;
    }
}
