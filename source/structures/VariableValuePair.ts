import Variable from 'Variable';

export default class VariableValuePair {
    public key: Variable;
    public value: number;
}
