import Variable from 'Variable';
import VariableValuePair from 'structures/VariableValuePair';

export default class VariableValuePairs {
    private table: { [key: string]: VariableValuePair; };
    private nElements: number;

    public constructor(itable?: VariableValuePair[]) {
        this.table = {};
        if (itable != null) {
            for (const vvp of itable) {
                this.table[vvp.key.id] = vvp;
            }
        }
        this.nElements = itable.length || 0;
    }

    public size(): number {
        return this.nElements;
    }

    public get(variable: Variable): number {
        return this.table[variable.id].value || null;
    }

    public keys(): Variable[] {
        const keys = new Array<Variable>();
        Object.values(this.table).forEach(vvp =>
            keys.push(vvp.key)
        );
        return keys;
        // Need ES2017 for this to work
        // return Object.keys<VariableValuePair>(this.table).map(key => this.table[key].key);
    }

    public values(): number[] {
        const vals = new Array<number>();
        Object.values(this.table).forEach(vvp =>
            vals.push(vvp.value)
        );
        return vals;
    }
}
