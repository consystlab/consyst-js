export default class PayloadQueue<T, S> {

  private queue: T[];
  private map: { [key: string]: S };

  constructor() {
    this.queue = new Array<T>();
    this.map = {};
  }

  public contains(item: T): boolean {
    return this.queue.indexOf(item) > -1;
  }

  public push(item: T, payload: S): void {
    this.queue.push(item);
    this.map[item.toString()] = payload;
  }

  public get(item: T): S {
    return this.map[item.toString()];
  }

  public pop(): [T, S] {
    const item = this.queue.shift();
    const payload = this.map[item.toString()];
    delete this.map[item.toString()];
    return [item, payload];
  }

  public peek(): [T, S] {
    return (this.queue[0] && [this.queue[0], this.map[this.queue[0].toString()]]) || null;
  }

  public isEmpty(): boolean {
    return this.queue.length === 0;
  }
}
