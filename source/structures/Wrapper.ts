class Wrapper<T> {
  constructor(private value: T) {}

  public get(): T {
    return this.value;
  }

  public set(value: T): void {
    this.value = value;
  }
}

export default Wrapper;
