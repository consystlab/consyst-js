import hrtime = require('browser-process-hrtime');

const NS_PER_SEC = 1e9;

class SearchAnalytics {
  public static getInstance() {
    if (!SearchAnalytics.instance) {
      SearchAnalytics.instance = new SearchAnalytics();
    }
    return SearchAnalytics.instance;
  }

  private static instance: SearchAnalytics;

  /* End of singleton pattern */

  public backtracks: number;
  public backtracksPerLevel: Array<number>;
  public nodesVisited: number;
  public constraintChecks: number;

  public startHRTime: [number, number];
  public totalHRTime: [number, number];

  private constructor() { }

  public initialize(numberOfVariables: number) {
    this.backtracks = 0;
    this.nodesVisited = 0;
    this.constraintChecks = 0;
    this.backtracksPerLevel = [];
    for (let i = 0; i <= numberOfVariables; i++) {
      this.backtracksPerLevel[i] = 0;
    }
  }

  public start() {
    this.startHRTime = hrtime();
  }

  public stop() {
    this.totalHRTime = hrtime(this.startHRTime);
  }

  public totalTime() {
    return `${this.totalHRTime[0]} sec ${this.totalHRTime[1]} nanoseconds`;
  }

  public reset() {
    this.backtracks = 0;
    this.nodesVisited = 0;
    if (!this.backtracksPerLevel) {
      this.backtracksPerLevel = [];
    }
    for (let i = 0; i <= this.backtracksPerLevel.length; i++) {
      this.backtracksPerLevel[i] = 0;
    }

    this.startHRTime = undefined;
    this.totalHRTime = undefined;
  }
}

export default SearchAnalytics;
