import Variable from 'Variable';

class SearchResult {
  constructor(
    public found: boolean,
    public solution?: { [key: string]: [Variable, number] },
  ) {}
}

export default SearchResult;
