export default class CyclicList<T> {

  public readonly length: number;

  private cPosition: number;

  constructor(private queue: Array<T> = new Array<T>()) {
    this.cPosition = 0;
    this.length = this.queue.length;
  }

  public contains(item: T): boolean {
    return this.queue.indexOf(item) > -1;
  }

  public next(): T {
    if (this.queue.length === 0) {
      return null;
    }
    const x = this.queue[this.cPosition];
    this.cPosition = (this.cPosition + 1) % this.queue.length;
    return x;
  }

  public getPosition(): number {
    return this.cPosition;
  }

  public isEmpty(): boolean {
    return this.queue.length === 0;
  }
}
