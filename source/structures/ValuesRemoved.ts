import Variable from 'Variable';

class ValuesRemoved {

  constructor(
    private removed: {
      [key: string]: {
        variable: Variable,
        removed: Set<number>
      }
    } = {},
    private constraintVariables: Set<Variable> = null,
  ) {
  }

  public initialize(variable: Variable, constraintVariables?: Set<Variable>) {
    if (!this.removed[variable.id]) {
      this.removed[variable.id] = {
        removed: new Set<number>(),
        variable,
      };
    }
    if (constraintVariables) {
      this.constraintVariables = constraintVariables;
    }
  }

  public getRemoved(variable: Variable): Set<number> {
    return this.removed[variable.id] && this.removed[variable.id].removed;
  }

  public setRemoved(variable: Variable, removed: Set<number>): void {
    this.removed[variable.id] = { variable, removed };
  }

  public addRemoved(variable: Variable, value: number) {
    if (!this.removed[variable.id]) {
      this.removed[variable.id] = {
        removed: new Set<number>(),
        variable,
      };
    }
    this.removed[variable.id].removed.add(value);
  }

  public addAllRemoved(variable: Variable, values: Set<number>) {
    if (!this.removed[variable.id]) {
      this.removed[variable.id] = {
        removed: new Set<number>(),
        variable,
      };
    }
    values.forEach(value =>
      this.removed[variable.id].removed.add(value)
    );
  }

  public getConstraintVariables() {
    return this.constraintVariables;
  }

  public setConstraintVariables(constraintVariables: Set<Variable>) {
    this.constraintVariables = constraintVariables;
  }

  public forEach(func: (variable: Variable, removed: Set<number>) => void) {
    Object.entries(this.removed).forEach(entry => {
      func(entry[1].variable, entry[1].removed);
    });
  }

  public reduceEntries<T>(
    func: (
      previousValue: T,
      entry: { variable: Variable, removed: Set<number>}
    ) => T,
    start: T = null,
  ): T {
    let previousValue = start;
    Object.values(this.removed).forEach(entry => {
      previousValue = func(previousValue, entry);
    });
    return previousValue;
  }

  public isEmpty(): boolean {
    return Object.entries(this.removed).every(entry => entry[1].removed.size === 0);
  }
}

export default ValuesRemoved;
