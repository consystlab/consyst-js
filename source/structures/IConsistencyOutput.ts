import ValuesRemoved from 'structures/ValuesRemoved';

interface IConsistencyOutput {
  removeHistory: ValuesRemoved[];
  consistent: boolean;
}

export default IConsistencyOutput;
