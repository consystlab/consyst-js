import Variable from 'Variable';
import Sudoku from 'Sudoku';
import IBinaryConstraint from 'constraints/binary/IBinaryConstraint';
import IGeneralConstraint from 'constraints/general/IGeneralConstraint';

abstract class VariableOrdering {
  public constructor(sudoku: Sudoku) {}
  public abstract next(): Variable;
  public abstract restore(levels?: number): void;
  public abstract getOrdering(): Variable[];
  public handleDomainWipeout(constraint: IBinaryConstraint | IGeneralConstraint): void {}
}

export default VariableOrdering;
