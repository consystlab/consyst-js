import Sudoku from 'Sudoku';
import Variable from 'Variable';
import VariableOrdering from 'orderings/variable/VariableOrdering';
import IBinaryConstraint from 'constraints/binary/IBinaryConstraint';
import IGeneralConstraint from 'constraints/general/IGeneralConstraint';

class DomWDegNonbinaryOrdering extends VariableOrdering {
  private constraintWeights: { [key: string]: number };
  private constraintSupports: { [key: string]: number };
  private variableWeights: { [key: string]: number };
  private ordering: Variable[];
  private level: number;

  constructor(sudoku: Sudoku) {
    super(sudoku);
    this.ordering = sudoku.variables.slice();
    this.level = 0;
    this.constraintWeights = {};
    this.variableWeights = {};
    this.constraintSupports = {};
    sudoku.generalConstraints.forEach(constraint => {
      this.constraintWeights[constraint.toString()] = 1;
      this.constraintSupports[constraint.toString()] = constraint.scope.length;
    });
    this.ordering.forEach(variable => {
      this.variableWeights[variable.id] = 0;
      variable.generalConstraints.forEach(constraint => {
        this.variableWeights[variable.id] += this.constraintWeights[constraint.toString()];
      });
    });
  }

  public next(): Variable {
    const variables = this.ordering.slice(this.level);
    for (let i = this.level; i < this.ordering.length; i++) {
      if (!this.ordering[i]) {
        const error = true;
      }
      if (this.ordering[i].domain.size() === 0) {
        return null;
      } else if (this.ordering[i].domain.size() === 1) {
        this.swap(this.level, i);
        return this.ordering[this.level++];
      }
    }

    let minDomWDeg = this.ordering[this.level];
    let minIndex = this.level;
    for (let i = this.level + 1; i < this.ordering.length; i++) {
      const orderNumber = this.ordering[i].domain.size() * this.variableWeights[minDomWDeg.toString()]
        - minDomWDeg.domain.size() * this.variableWeights[this.ordering[i].toString()];
      if (orderNumber === 0 && this.ordering[i].id.localeCompare(minDomWDeg.id) < 0) {
        minIndex = i;
        minDomWDeg = this.ordering[minIndex];
      } else if (orderNumber < 0) {
        minIndex = i;
        minDomWDeg = this.ordering[minIndex];
      }
    }

    this.swap(this.level, minIndex);
    this.removeVariable(this.level);
    return this.ordering[this.level++];
  }

  public restore(levels?: number): void {
    this.level -= (levels || 1);
    if (this.level > 0) {
      const variable = this.ordering[this.level];
      variable.generalConstraints.forEach(c => {
        this.constraintSupports[c.toString()] += 1;
        if (this.constraintSupports[c.toString()] === 2) {
          c.scope.forEach(scopeVariable => {
            this.variableWeights[scopeVariable.id] += this.constraintWeights[c.toString()];
          });
        }
      });
    }
  }

  public getOrdering(): Variable[] {
    return this.ordering;
  }

  public handleDomainWipeout(constraint: IBinaryConstraint | IGeneralConstraint): void {
    this.constraintWeights[constraint.toString()]++;
    if (this.constraintSupports[constraint.toString()] > 1) {
      constraint.scope.forEach(scopeVariable => {
        this.variableWeights[scopeVariable.id]++;
      });
    }
  }

  private removeVariable(index: number): void {
    const variable = this.ordering[index];
    if (!variable) {
      const error = true;
    }
    variable.generalConstraints.forEach(c => {
      this.constraintSupports[c.toString()] -= 1;
      if (this.constraintSupports[c.toString()] === 1) {
        c.scope.forEach(scopeVariable => {
          this.variableWeights[scopeVariable.id] -= this.constraintWeights[c.toString()];
        });
      }
    });
  }

  private swap(index1: number, index2: number): void {
    const temp = this.ordering[index1];
    this.ordering[index1] = this.ordering[index2];
    this.ordering[index2] = temp;
  }
}

export default DomWDegNonbinaryOrdering;
