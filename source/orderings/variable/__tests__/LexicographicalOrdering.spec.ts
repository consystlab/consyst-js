import SparseSet from 'domains/SparseSet';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import LexicographicalOrdering from 'orderings/variable/LexicographicalOrdering';

describe('lexicographicalOrdering', () => {
  let variables: Array<Variable>;
  let ordering: LexicographicalOrdering;

  beforeEach(() => {
    variables = [
      new Variable(new SparseSet([1, 2, 3]), 'b'),
      new Variable(new SparseSet([1, 2]), 'a'),
      new Variable(new SparseSet([1]), 'c'),
    ];
    const sudoku = new Sudoku(variables);
    ordering = new LexicographicalOrdering(sudoku);
  });

  it('orders lexicographically', () => {
    const first = ordering.next();
    const second = ordering.next();
    const third = ordering.next();
    expect(first.id).toEqual('a');
    expect(second.id).toEqual('b');
    expect(third.id).toEqual('c');
  });
});
