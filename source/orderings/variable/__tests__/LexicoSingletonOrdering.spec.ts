import SparseSet from 'domains/SparseSet';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import LexicoSingletonOrdering from 'orderings/variable/LexicoSingletonOrdering';
import VariableOrdering from 'orderings/variable/VariableOrdering';

describe('lexicographicalOrdering', () => {
  let variables: Array<Variable>;
  let ordering: VariableOrdering;

  beforeEach(() => {
    variables = [
      new Variable(new SparseSet([1, 2, 3]), 'b'),
      new Variable(new SparseSet([1, 2]), 'a'),
      new Variable(new SparseSet([1]), 'c'),
    ];
    const sudoku = new Sudoku(variables);
    ordering = new LexicoSingletonOrdering(sudoku);
  });

  it('puts in singleton then lexicographical ordering', () => {
    const first = ordering.next();
    const second = ordering.next();
    const third = ordering.next();
    expect(first.id).toEqual('c');
    expect(second.id).toEqual('a');
    expect(third.id).toEqual('b');
  });

  it('Restores and compute singleton dynamically', () => {
    ordering.next();
    ordering.next();
    ordering.next();

    ordering.restore(2);
    variables[0].domain.remove(1);
    variables[0].domain.remove(2);

    const second = ordering.next();
    const third = ordering.next();
    expect(second.id).toEqual('b');
    expect(third.id).toEqual('a');
  });
});
