import SparseSet from 'domains/SparseSet';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import BinaryAllDiff from 'constraints/binary/BinaryAllDiff';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';
import DomWDegOrdering from 'orderings/variable/DomWDegOrdering';

describe('DomWDegOrdering', () => {
  let variables: Array<Variable>;
  let constraints: Array<BinaryAllDiff> | Array<GeneralAllDiff>;
  let ordering: DomWDegOrdering;

  it('orders smallest to largest domain with no constraint weights', () => {
    variables = [
      new Variable(new SparseSet([1, 2, 3]), 'a'),
      new Variable(new SparseSet([1, 2]), 'b'),
      new Variable(new SparseSet([1]), 'c'),
    ];
    constraints = [
      new GeneralAllDiff([variables[0]]),
      new GeneralAllDiff([variables[1]]),
      new GeneralAllDiff([variables[2]]),
    ];
    const sudoku = new Sudoku(variables, [], constraints);
    ordering = new DomWDegOrdering(sudoku);

    const first = ordering.next();
    const second = ordering.next();
    const third = ordering.next();
    expect(first.id).toEqual('c');
    expect(second.id).toEqual('b');
    expect(third.id).toEqual('a');
  });

  it('takes into account the weight of a constraint', () => {
    variables = [
      new Variable(new SparseSet([1]), 'a'),
      new Variable(new SparseSet([1, 2]), 'b'),
      new Variable(new SparseSet([1, 2, 3]), 'c'),
      new Variable(new SparseSet([1, 2, 3, 4]), 'd'),
      new Variable(new SparseSet([1, 2, 3, 4, 5]), 'e'),
    ];
    constraints = [
      new GeneralAllDiff([variables[0], variables[1]]),
      new GeneralAllDiff([variables[1], variables[2]]),
      new GeneralAllDiff([variables[1], variables[3]]),
      new GeneralAllDiff([variables[1], variables[4]]),
      new GeneralAllDiff([variables[2], variables[4]]),
    ];
    const sudoku = new Sudoku(variables, [], constraints);
    ordering = new DomWDegOrdering(sudoku);

    for (let i = 0; i < 10; i++) {
      ordering.handleDomainWipeout(constraints[4]);
    }
    const first = ordering.next();
    const second = ordering.next();
    const third = ordering.next();
    const fourth = ordering.next();
    const fifth = ordering.next();
    expect(first.id).toEqual('a');
    expect(second.id).toEqual('c');
    expect(third.id).toEqual('b');
    expect(fourth.id).toEqual('d');
    expect(fifth.id).toEqual('e');
  });
});
