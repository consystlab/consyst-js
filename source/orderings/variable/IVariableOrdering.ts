import Sudoku from 'Sudoku';
import VariableOrdering from 'orderings/variable/VariableOrdering';

interface IVariableOrdering {
  new(sudoku: Sudoku): VariableOrdering;
}

export default IVariableOrdering;
