import Sudoku from 'Sudoku';
import Variable from 'Variable';
import IVariableOrdering from 'orderings/variable/IVariableOrdering';
import VariableOrdering from 'orderings/variable/VariableOrdering';

class LexicoSingletonOrdering extends VariableOrdering {
  private ordering: Variable[];
  private level: number;

  constructor(sudoku: Sudoku) {
    super(sudoku);
    this.ordering = sudoku.variables.slice();
    this.level = 0;
  }

  public next(): Variable {
    for (let i = this.level; i < this.ordering.length; i++) {
      if (this.ordering[i].domain.size() === 1) {
        this.swap(this.level, i);
        return this.ordering[this.level++];
      }
    }
    let minIndex = this.level;
    let minLexico = this.ordering[this.level].id;
    for (let i = this.level + 1; i < this.ordering.length; i++) {
      if (this.ordering[i].id.localeCompare(minLexico) < 0) {
        minIndex = i;
        minLexico = this.ordering[i].id;
      }
    }
    this.swap(this.level, minIndex);
    return this.ordering[this.level++];
  }

  public restore(levels?: number): void {
    this.level -= (levels || 1);
  }

  public getOrdering(): Variable[] {
    return this.ordering;
  }

  private swap(index1: number, index2: number): void {
    const temp = this.ordering[index1];
    this.ordering[index1] = this.ordering[index2];
    this.ordering[index2] = temp;
  }
}

export default LexicoSingletonOrdering;
