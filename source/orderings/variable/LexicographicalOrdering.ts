import Sudoku from 'Sudoku';
import Variable from 'Variable';
import VariableOrdering from 'orderings/variable/VariableOrdering';

class LexicographicalOrdering extends VariableOrdering {
  private ordering: Variable[];
  private level: number;

  constructor(sudoku: Sudoku) {
    super(sudoku);
    this.ordering = this.order(sudoku.variables.slice());
    this.level = 0;
  }

  public next(): Variable {
    return this.ordering[this.level++];
  }

  public restore(levels?: number): void {
    this.level -= (levels || 1);
  }

  public getOrdering(): Variable[] {
    return this.ordering;
  }

  private order(variables: Variable[]): Variable[] {
    const copy = variables.slice();
    copy.sort((a, b) => {
      return a.id.localeCompare(b.id);
    });
    return copy;
  }
}

export default LexicographicalOrdering;
