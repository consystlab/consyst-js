import IDomain from 'domains/IDomain';
import ValuesRemoved from 'structures/ValuesRemoved';
import Queue from 'structures/Queue';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import Wrapper from 'structures/Wrapper';
import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import GeneralAlldiff from 'constraints/general/GeneralAllDiff';

const checkPropGac = (sudoku: Sudoku, dataWrapper: Wrapper<string>) => {
  let error = '';
  sudoku.generalConstraints.forEach((constraint) => {
    const alldiff = constraint as GeneralAlldiff;
    const valueGraph = alldiff.getValueGraph();
    const matching = alldiff.getMatching();
    if (valueGraph) {
      valueGraph.edges.forEach(edge => {
        const variable = alldiff.scope.find(x => x === edge.left.value);
        if (!variable) {
          error += `Could not find: ${edge.left.value.id} in scope of ${alldiff.toString()}\r\n`;
        }
        if (!variable.domain.contains(edge.right.value)) {
          error += `Variable ${variable.id} from constraint` +
            `${alldiff.toString()} does not have ${edge.right.value} in its domain\r\n`;
        }
      });
      alldiff.scope.forEach(variable => {
        variable.domain.current().forEach(element => {
          if (!valueGraph.edges.find(x => x.left.value === variable && x.right.value === element)) {
            error += `VVP: (${variable.id}, ${element}) not found in constraint ${alldiff.toString()}\r\n`;
          }
        });
      });
    }
  });
  if (error) {
    throw new Error('CheckPropGac found an error, exiting.\r\n');
  }
};

export default checkPropGac;
