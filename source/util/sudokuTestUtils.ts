import Variable from 'Variable';
import BinaryAllDiff from 'constraints/binary/BinaryAllDiff';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';
import Sudoku from 'Sudoku';
import SparseSet from 'domains/SparseSet';

const sudokuLoader = (sudokuString: string) => {
  const nums = sudokuString.split('').map(v => parseInt(v, 10));
  const values = new Array<Array<number>>();
  nums.forEach((num, i) => {
    const x = i % 9;
    const y = (i - x) / 9;
    if (values[y] == null) {
      values[y] = [];
    }
    values[y][x] = num;
  });

  const width = 9;
  const height = 9;
  const groupx = 3;
  const groupy = 3;

  const variables = new Array<Array<Variable>>();
  const binaryConstraints = new Array<BinaryAllDiff>();
  const generalConstraints = new Array<GeneralAllDiff>();
  const arcGroups = new Array<Array<Variable>>();

  for (let i = 0; i < height; i++) {
    variables[i] = [];
    for (let j = 0; j < width; j++) {
      const variable = new Variable(new SparseSet([
        1, 2, 3, 4, 5, 6, 7, 8, 9,
      ]), `${i}_${j}`);
      if (values[i][j] !== 0) {
        variable.domain.current().forEach(d => {
          if (d !== values[i][j]) {
            variable.domain.remove(d);
          }
        });
        variable.assign(values[i][j]);
      }
      variables[i][j] = variable;
    }
  }

  // Add binary constraints (vertical)
  for (let i = 0; i < width; i++) {
    for (let j = 0; j < height - 1; j++) {
      for (let k = j + 1; k < height; k++) {
        binaryConstraints.push(new BinaryAllDiff([variables[j][i], variables[k][i]]));
      }
    }
  }
  // Add binary constraints (horizantal)
  for (let i = 0; i < height; i++) {
    for (let j = 0; j < width - 1; j++) {
      for (let k = j + 1; k < width; k++) {
        binaryConstraints.push(new BinaryAllDiff([variables[i][j], variables[i][k]]));
      }
    }
  }

  // Variable groups
  const xgroups = width / groupx;
  const ygroups = height / groupy;
  for (let i = 0; i < xgroups * ygroups; i++) {
    const group = [];
    for (let j = 0; j < groupx * groupy; j++) {
      const x = (i % xgroups) * groupx + j % groupx;
      const y = Math.floor(i / xgroups) * groupy + Math.floor(j / groupx);
      group.push(variables[y][x]);
    }
    arcGroups[i] = group;
  }

  // Add binary constraints (group)
  for (let i = 0; i < arcGroups.length; i++) {
    for (let j = 0; j < arcGroups[i].length - 1; j++) {
      for (let k = j + 1; k < arcGroups[i].length; k++) {
        binaryConstraints.push(
          new BinaryAllDiff([arcGroups[i][j], arcGroups[i][k]]),
        );
      }
    }
  }

  // Add general constraints (vertical)
  for (let i = 0; i < width; i++) {
    const scope = [];
    for (let j = 0; j < height; j++) {
      scope.push(variables[j][i]);
    }
    const gconstraint = new GeneralAllDiff(scope);
    generalConstraints.push(gconstraint);
  }

  // Add general constraints (horizantal)
  for (let i = 0; i < height; i++) {
    const scope = [];
    for (let j = 0; j < width; j++) {
      scope.push(variables[i][j]);
    }
    const gconstraint = new GeneralAllDiff(scope);
    generalConstraints.push(gconstraint);
  }

  // Add general constraints (group)
  for (let i = 0; i < arcGroups.length; i++) {
    const gconstraint = new GeneralAllDiff(arcGroups[i]);
    generalConstraints.push(gconstraint);
  }

  const variableList = variables.reduce((acc, line) => { acc.push(...line); return acc; }, []);
  const sudoku = new Sudoku(variableList, binaryConstraints, generalConstraints);
  return sudoku;
};

const toHaveSingularDomain = (variable: Variable) => {
  const singularDomain = variable.domain.size() === 1;
  return {
    message: () => (
      `Expected variable ${variable.id} to have domain 1: received ${variable.domain.current().sort()}`
    ),
    pass: singularDomain,
  };
};

export default {
  sudokuLoader,
  toHaveSingularDomain,
};
