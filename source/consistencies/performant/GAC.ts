import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import ValuesRemoved from 'structures/ValuesRemoved';
import PayloadQueue from 'structures/PayloadQueue';
import Sudoku from 'Sudoku';
import Variable from 'Variable';

const GAC = (
  sudoku: Sudoku,
): ValuesRemoved => {
  const totalRemoved = new ValuesRemoved();
  const payloadQueue = new PayloadQueue<IGeneralConstraint, ValuesRemoved>();
  sudoku.generalConstraints.forEach(constraint => {
    payloadQueue.push(constraint, new ValuesRemoved());
  });
  let consistent = true;
  while (!payloadQueue.isEmpty() && consistent) {
    const [constraint, valuesRemoved] = payloadQueue.pop();
    const { valid, reduction } = constraint.diffPropagation(valuesRemoved);
    if (!valid) {
      consistent = false;
    }
    reduction.forEach((variable, removedValues) => {
      totalRemoved.initialize(variable);
      removedValues.forEach(removedValue => {
        if (variable.isAssigned() || !variable.domain.remove(removedValue)) {
          consistent = false;
        } else {
          totalRemoved.addRemoved(variable, removedValue);
          if (variable.domain.size() === 0) {
            consistent = false;
          }
        }
      });
      variable.generalConstraints.forEach(neighborConstraint => {
        if (neighborConstraint !== constraint) {
          if (payloadQueue.contains(neighborConstraint)) {
            payloadQueue.get(neighborConstraint).addAllRemoved(variable, removedValues);
          } else {
            payloadQueue.push(
              neighborConstraint,
              new ValuesRemoved({
                [variable.id]: {
                  variable,
                  removed: removedValues,
                },
              })
            );
          }
        }
      });
    });
  }
  return totalRemoved;
};

export default GAC;
