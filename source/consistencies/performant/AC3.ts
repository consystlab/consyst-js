import IBinaryConstraint from 'constraints/binary/IBinaryConstraint';
import ValuesRemoved from 'structures/ValuesRemoved';
import Queue from 'structures/Queue';
import Sudoku from 'Sudoku';
import Variable from 'Variable';

// Returns changed variables and the values removed from their domains
const AC3 = (
  sudoku: Sudoku,
  subset?: Variable[]
): ValuesRemoved => {
  const removed = new ValuesRemoved();
  if (subset) {
    const queue = new Queue<[Variable, Variable]>();
    for (let i = 0; i < subset.length; i++) {
      if (!subset[i].isAssigned()) {
        subset[i].binaryNeighbors.forEach(neighbor => {
          if (subset.indexOf(neighbor) > -1) {
            queue.push([subset[i], neighbor]);
          }
        });
      }
    }

    while (!queue.isEmpty()) {
      const tuple = queue.pop();
      const original = new Set<number>(tuple[0].domain.current());
      if (tuple[0].binaryRevise(tuple[1])) {
        const currentDomain = tuple[0].domain.current();
        for (let i = 0; i < currentDomain.length; i++) {
          if (original.has(currentDomain[i])) {
            original.delete(currentDomain[i]);
          }
        }
        if (!removed.getRemoved(tuple[0]) && original.size > 0) {
          removed.setRemoved(tuple[0], new Set<number>());
        }
        const removedValues = new Set<number>();
        original.forEach(val => {
          removed.addRemoved(tuple[0], val);
          removedValues.add(val);
        });

        tuple[0].binaryNeighbors.forEach(neighbor => {
          if (subset.indexOf(neighbor) > -1) {
            queue.push([neighbor, tuple[0]]);
          }
        });

        if (!tuple[0].domain.size()) {
          break;
        }
      }
    }
  } else {
    const queue = new Queue<[Variable, Variable]>();
    for (let i = 0; i < sudoku.variables.length; i++) {
      sudoku.variables[i].binaryNeighbors.forEach(neighbor => queue.push([sudoku.variables[i], neighbor]));
    }
    while (!queue.isEmpty()) {
      const tuple = queue.pop();
      const original = new Set<number>(tuple[0].domain.current());
      if (tuple[0].binaryRevise(tuple[1])) {
        const currentDomain = tuple[0].domain.current();
        for (let i = 0; i < currentDomain.length; i++) {
          if (original.has(currentDomain[i])) {
            original.delete(currentDomain[i]);
          }
        }

        if (!removed.getRemoved(tuple[0]) && original.size > 0) {
          removed.initialize(tuple[0]);
        }

        const removedValues = new Set<number>();

        original.forEach(val => {
          removed.addRemoved(tuple[0], val);
          removedValues.add(val);
        });

        tuple[0].binaryNeighbors.forEach(neighbor => queue.push([neighbor, tuple[0]]));

        if (!tuple[0].domain.size()) {
          break;
        }
      }
    }
  }
  return removed;
};

export default AC3;
