import IBinaryConstraint from 'constraints/binary/IBinaryConstraint';
import ValuesRemoved from 'structures/ValuesRemoved';
import Queue from 'structures/Queue';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import VariableValuePairs from 'structures/VariableValuePairs';

// Returns changed variables and the values removed from their domains
const verifyConsistency = (
  sudoku: Sudoku
): boolean => {
  const errors = new Set<Variable>();

  sudoku.binaryConstraints.forEach(constraint => {
    if (constraint.scope[0].domain.size() === 1 && constraint.scope[1].domain.size() === 1) {
      if (!constraint.check(new VariableValuePairs([
        {
          key: constraint.scope[0],
          value: constraint.scope[0].domain.first(),
        }, {
          key: constraint.scope[1],
          value: constraint.scope[1].domain.first(),
        },
      ]))) {
        errors.add(constraint.scope[0]);
        errors.add(constraint.scope[1]);
      }
    }
  });

  return errors.size === 0;
};

export default verifyConsistency;
