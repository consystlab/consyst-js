import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import ValuesRemoved from 'structures/ValuesRemoved';
import IConsistencyOutput from 'structures/IConsistencyOutput';
import SparseSet from 'domains/SparseSet';
import IDomain from 'domains/IDomain';
import Queue from 'structures/Queue';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import CyclicList from 'structures/CyclicList';
import PayloadQueue from 'structures/PayloadQueue';

let counter: { [key: string]: Array<number> };

const restoreDomains = (removal: ValuesRemoved, update: boolean) => {
  if (update) {
    removal.forEach((variable, removed ) => {
      removed.forEach(value => {
        variable.domain.restore(value);
        if (!counter[variable.id]) {
          counter[variable.id] = [];
        }
        if (!counter[variable.id][value]) {
          counter[variable.id][value] = 0;
        }
        counter[variable.id][value]++;
      });
    });
  } else {
    removal.forEach((variable, removed) => {
      removed.forEach(value => {
        variable.domain.restore(value);
      });
    });
  }
};

const testGAC = (
  startingVariable: Variable,
  value: number,
  sudoku: Sudoku,
): {
  consistent: boolean,
} => {
  let consistent = true;
  const removal = new ValuesRemoved();
  const payloadQueue = new PayloadQueue<IGeneralConstraint, ValuesRemoved>();
  startingVariable.assign(value);
  sudoku.generalConstraints.forEach(c => c.resetPropagation());
  const startingRemoved = startingVariable.domain.current().filter(x => x !== value);
  startingVariable.generalConstraints.forEach(constraint => {
    payloadQueue.push(constraint, new ValuesRemoved({
      [startingVariable.id]: {
        variable: startingVariable,
        removed: new Set(startingRemoved),
      },
    }));
  });
  while (!payloadQueue.isEmpty() && consistent) {
    const [constraint, valuesRemoved] = payloadQueue.pop();
    const { valid, reduction } = constraint.diffPropagation(valuesRemoved);
    if (!valid) {
      consistent = false;
    }
    reduction.forEach((variable, removedValues) => {
      removal.initialize(variable);
      removedValues.forEach(removedValue => {
        if (variable.isAssigned() || !variable.domain.remove(removedValue)) {
          consistent = false;
        } else {
          removal.addRemoved(variable, removedValue);
          if (variable.domain.size() === 0) {
            consistent = false;
          }
        }
      });
      variable.generalConstraints.forEach(neighborConstraint => {
        if (neighborConstraint !== constraint) {
          if (payloadQueue.contains(neighborConstraint)) {
            payloadQueue.get(neighborConstraint).addAllRemoved(variable, removedValues);
          } else {
            payloadQueue.push(
              neighborConstraint,
              new ValuesRemoved({
                [variable.id]: {
                  variable,
                  removed: removedValues,
                },
              })
            );
          }
        }
      });
    });
  }
  startingVariable.unassign();
  startingRemoved.forEach(v => startingVariable.domain.restore(v));
  restoreDomains(removal, consistent);
  return { consistent };
};

const varPOGAC = (
  startingVariable: Variable,
  sudoku: Sudoku,
): { consistent: boolean, change: boolean, removals: ValuesRemoved[] } => {
  let size = startingVariable.domain.size();
  let change = false;
  const removals = new Array<ValuesRemoved>();

  counter = {};
  let domain = startingVariable.domain.current();
  for (let i = 0; i < domain.length; i++) {
    const test = testGAC(startingVariable, domain[i], sudoku);
    if (!test.consistent) {
      startingVariable.domain.remove(domain[i]);
      removals.push(new ValuesRemoved({ [startingVariable.id]: {
        variable: startingVariable,
        removed: new Set([domain[i]]),
      }}));
      const enforce = sudoku.generalArcConsistency();
      removals.push.apply(removals, enforce.removeHistory);
      if (!enforce.consistent) {
        return {
          consistent: false,
          change: true,
          removals,
        };
      }
    }
  }
  if (startingVariable.domain.size() === 0) {
    return {
      consistent: false,
      change: true,
      removals,
    };
  }
  if (size !== startingVariable.domain.size()) {
    change = true;
  }
  const symmetricRemoval = new ValuesRemoved();
  for (let i = 0; i < sudoku.variables.length; i++) {
    const variable = sudoku.variables[i];
    if (variable === startingVariable) {
      continue;
    }
    size = variable.domain.size();
    domain = variable.domain.current();
    for (let j = 0; j < domain.length; j++) {
      const c = (counter[variable.id] && counter[variable.id][domain[j]]) || 0;
      if (c === startingVariable.domain.size()) {
        variable.domain.remove(domain[j]);
        symmetricRemoval.addRemoved(variable, domain[j]);
      }
    }
    if (variable.domain.size() === 0) {
      removals.push(symmetricRemoval);
      return {
        consistent: false,
        change,
        removals,
      };
    }
    if (variable.domain.size() !== size) {
      change = true;
    }
  }
  if (!symmetricRemoval.isEmpty()) {
    removals.push(symmetricRemoval);
  }
  return {
    consistent: true,
    change,
    removals,
  };
};

const POGAC = (
  sudoku: Sudoku
): IConsistencyOutput => {
  const arcConsistency = sudoku.generalArcConsistency();
  if (!arcConsistency.consistent) {
    return arcConsistency;
  }
  const removeHistory = arcConsistency.removeHistory;
  let consistent = true;
  const s = new CyclicList(sudoku.variables);

  let fpp = 0;
  let x = s.next();
  const cycleLength = s.length;
  while (fpp < cycleLength && consistent) {
    let change = false;
    if (!x.isAssigned() && x.domain.size() > 1) {
      const varPoacRun = varPOGAC(x, sudoku);
      consistent = varPoacRun.consistent;
      change = varPoacRun.change;
      removeHistory.push.apply(removeHistory, varPoacRun.removals);
      if (!consistent) {
        break;
      }
    }
    if (change) {
      fpp = 1;
    } else {
      fpp++;
    }
    x = s.next();
  }

  return {
    removeHistory,
    consistent,
  };
};

export default POGAC;
