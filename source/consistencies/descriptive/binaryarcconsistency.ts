import IBinaryConstraint from 'constraints/binary/IBinaryConstraint';
import ValuesRemoved from 'structures/ValuesRemoved';
import Queue from 'structures/Queue';
import IConsistencyOutput from 'structures/IConsistencyOutput';
import Sudoku from 'Sudoku';
import Variable from 'Variable';

// Returns changed variables and the values removed from their domains
const AC3 = (
  sudoku: Sudoku,
  subset?: Variable[]
): IConsistencyOutput => {
  const removed = new ValuesRemoved();
  const removeHistory = new Array<ValuesRemoved>();
  let consistent = true;
  if (subset) {
    const queue = new Queue<[Variable, Variable]>();
    subset.forEach(var1 => {
      if (!var1.isAssigned()) {
        var1.binaryNeighbors.forEach(var2 => {
          if (subset.indexOf(var2) > -1) {
            queue.push([var1, var2]);
          }
        });
      }
    });

    while (!queue.isEmpty()) {
      const tuple = queue.pop();
      const original = new Set<number>(tuple[0].domain.current());
      if (tuple[0].binaryRevise(tuple[1])) {
        tuple[0].domain.current().forEach(val => {
          if (original.has(val)) {
            original.delete(val);
          }
        });
        if (!removed.getRemoved(tuple[0]) && original.size > 0) {
          removed.setRemoved(tuple[0], new Set<number>());
        }
        const removedValues = new Set<number>();
        original.forEach(val => {
          removed.addRemoved(tuple[0], val);
          removedValues.add(val);
        });

        const removal = new ValuesRemoved(
          {
            [tuple[0].id]: {
              removed: removedValues,
              variable: tuple[0],
            },
          },
          new Set<Variable>([tuple[0], tuple[1]]),
        );
        removeHistory.push(removal);

        tuple[0].binaryNeighbors.forEach(neighbor => {
          if (subset.indexOf(neighbor) > -1) {
            queue.push([neighbor, tuple[0]]);
          }
        });

        if (!tuple[0].domain.size()) {
          consistent = false;
          break;
        }
      }
    }
  } else {
    const queue = new Queue<[Variable, Variable]>();
    sudoku.variables.forEach(var1 => {
      var1.binaryNeighbors.forEach(var2 => {
        queue.push([var1, var2]);
      });
    });
    while (!queue.isEmpty()) {
      const tuple = queue.pop();
      const original = new Set<number>(tuple[0].domain.current());
      if (tuple[0].binaryRevise(tuple[1])) {
        tuple[0].domain.current().forEach(val => {
          if (original.has(val)) {
            original.delete(val);
          }
        });

        if (!removed.getRemoved(tuple[0]) && original.size > 0) {
          removed.initialize(tuple[0]);
        }

        const removedValues = new Set<number>();
        original.forEach(val => {
          removed.addRemoved(tuple[0], val);
          removedValues.add(val);
        });

        const removal = new ValuesRemoved(
          {
            [tuple[0].id]: {
              removed: removedValues,
              variable: tuple[0],
            },
          },
          new Set<Variable>([tuple[0], tuple[1]]),
        );
        removeHistory.push(removal);

        tuple[0].binaryNeighbors.forEach(neighbor => {
          queue.push([neighbor, tuple[0]]);
        });

        if (!tuple[0].domain.size()) {
          consistent = false;
          break;
        }
      }
    }
  }
  return { removeHistory, consistent };
};

export default AC3;
