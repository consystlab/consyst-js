import ValuesRemoved from 'structures/ValuesRemoved';
import Queue from 'structures/Queue';
import IConsistencyOutput from 'structures/IConsistencyOutput';
import Variable from 'Variable';

// The first variable in the set must be the modified variable
const BinaryForwardChecking = (
  set: Variable[]
): IConsistencyOutput => {
  const removed = new ValuesRemoved();
  const removeHistory = new Array<ValuesRemoved>();
  const queue = new Queue<[Variable, Variable]>();
  const var1 = set[0];
  let consistent = true;
  for (const var2 of Array.from(var1.binaryNeighbors)) {
    if (set.indexOf(var2) > -1 && !var2.isAssigned()) {
      queue.push([var2, var1]);
    }
  }
  while (!queue.isEmpty()) {
    const tuple = queue.pop();
    const original = new Set(tuple[0].domain.current());
    if (tuple[0].binaryRevise(tuple[1])) {
      const after = tuple[0].domain.current();
      if (tuple[0].domain.size() === 0) {
        consistent = false;
      }
      after.forEach(val => {
        if (original.has(val)) {
          original.delete(val);
        }
      });
      if (original.size > 0) {
        removed.setRemoved(tuple[0], original);
        const removal = new ValuesRemoved();
        removal.setRemoved(tuple[0], original);
        removal.setConstraintVariables(new Set<Variable>([tuple[0], tuple[1]]));
        removeHistory.push(removal);
      }
    }
  }
  return { removeHistory, consistent };
};

export default BinaryForwardChecking;
