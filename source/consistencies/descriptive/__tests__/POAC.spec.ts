import SparseSet from 'domains/SparseSet';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import BinaryAllDiff from 'constraints/binary/BinaryAllDiff';
import VariableValuePairs from 'structures/VariableValuePairs';
import VariableValuePair from 'structures/VariableValuePair';

import TestUtils from 'util/sudokuTestUtils';

const JULLIERAT1 = '001070600920004000000003000009000001370080900050000360005010006000008050000507040';
const FEB14 = '075000000400003000308000607080007306010906050607300080701000403000400008000000510';

describe('BiSAC', () => {
  let sudoku: Sudoku;

  [
    ['Jullierat1', JULLIERAT1],
    ['Feb-14-2010', FEB14],
  ].forEach(problem => {
    it(`Solves ${problem[0]}`, () => {
      sudoku = TestUtils.sudokuLoader(problem[1]);
      const poacRun = sudoku.poac();

      expect(poacRun.consistent).toBe(true);
      const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
      expect(solved).toBe(true);
    });
  });
});
