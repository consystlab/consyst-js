import SparseSet from 'domains/SparseSet';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import BinaryAllDiff from 'constraints/binary/BinaryAllDiff';
import VariableValuePairs from 'structures/VariableValuePairs';
import VariableValuePair from 'structures/VariableValuePair';

describe('SingletonArcConsistency', () => {
  it('Solves a SAC problem', () => {
    const variable1 = new Variable(new SparseSet([1, 2, 3]), 'a');
    const variable2 = new Variable(new SparseSet([1, 2]), 'b');
    const constraint = new BinaryAllDiff([variable1, variable2]);

    const revision1 = variable1.binaryRevise(variable2);
    expect(revision1).toEqual(false);
    const revision2 = variable2.binaryRevise(variable1);
    expect(revision2).toEqual(false);

    variable2.domain.remove(2);
    const revision3 = variable1.binaryRevise(variable2);
    expect(revision3).toEqual(true);
    expect(variable1.domain.current().sort()).toEqual([2, 3]);
    const revision4 = variable2.binaryRevise(variable1);
    expect(revision4).toEqual(false);
  });
});
