import SparseSet from 'domains/SparseSet';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import BinaryAllDiff from 'constraints/binary/BinaryAllDiff';
import VariableValuePairs from 'structures/VariableValuePairs';
import VariableValuePair from 'structures/VariableValuePair';

import TestUtils from 'util/sudokuTestUtils';

const MARCH18 = '000650400021003080000000390002000608000030000406000200098000000010300920003019000';
const BROKEN_BRICK = '400060070000000600030002001700008500010400000020950000000000705009100030003040080';
const ESCARGOT = '100007090030020008009600500005300900010080002600004000300000010040000007007000300';

describe('BiSGAC', () => {
  let sudoku: Sudoku;

  [
    ['March18-2010', MARCH18],
    ['AI Broken Brick', BROKEN_BRICK],
    ['AI Escargot', ESCARGOT],
  ].forEach(problem => {
    it(`Solves ${problem[0]}`, () => {
      sudoku = TestUtils.sudokuLoader(problem[1]);
      const { consistent } = sudoku.bisgac();

      expect(consistent).toEqual(true);
      const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
      expect(solved).toBe(true);
    });
  });
});
