import SparseSet from 'domains/SparseSet';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import BinaryAllDiff from 'constraints/binary/BinaryAllDiff';
import VariableValuePairs from 'structures/VariableValuePairs';
import VariableValuePair from 'structures/VariableValuePair';

import TestUtils from 'util/sudokuTestUtils';

const AI_ESCARGOT = '100007090030020008009600500005300900010080002600004000300000010040000007007000300';
const AI_LUCKY_DIAMOND = '100500400009030000070008005001000030800600500090007008004020010200800600000001002';
const AI_CIRCLES = '005009700060000020100800006010700004007060030600003200000006040090050100800100002';

describe('SSGAC', () => {
  let sudoku: Sudoku;

  it('Solves AI Escargot', () => {
    sudoku = TestUtils.sudokuLoader(AI_ESCARGOT);
    sudoku.ssgac();

    const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
    expect(solved).toBe(true);
  });

  it('Solves AI Lucky Diamond', () => {
    sudoku = TestUtils.sudokuLoader(AI_LUCKY_DIAMOND);
    sudoku.ssgac();

    const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
    expect(solved).toBe(true);
  });

  it('Solves AI Circles', () => {
    sudoku = TestUtils.sudokuLoader(AI_CIRCLES);
    sudoku.ssgac();

    const solved = sudoku.variables.every(variable => variable.domain.size() === 1);
    expect(solved).toBe(true);
  });
});
