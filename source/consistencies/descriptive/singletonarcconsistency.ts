import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import ValuesRemoved from 'structures/ValuesRemoved';
import IConsistencyOutput from 'structures/IConsistencyOutput';
import Queue from 'structures/Queue';
import Sudoku from 'Sudoku';
import Variable from 'Variable';

const SingletonArcConsistency = (
  sudoku: Sudoku,
  subset?: Variable[]
): IConsistencyOutput => {
  const binaryConsistency = sudoku.binaryArcConsistency(subset);
  if (!binaryConsistency.consistent) {
    return binaryConsistency;
  }
  const removeHistory = binaryConsistency.removeHistory;
  const totalRemoved = new ValuesRemoved();
  let consistent = true;
  removeHistory.forEach(step => {
    step.forEach((variable, removed) => {
      totalRemoved.initialize(variable);
      removed.forEach(removedValue => {
        totalRemoved.addRemoved(variable, removedValue);
      });
    });
  });
  const queue = new Queue<Variable>();
  sudoku.variables.forEach(variable => {
    if (!variable.isAssigned() && variable.domain.size() > 1) {
      queue.push(variable);
    }
  });
  while (!queue.isEmpty() && consistent) {
    const variable = queue.pop();
    if (variable.domain.size() === 1 || variable.isAssigned()) {
      continue;
    }
    const currentDomain = variable.domain.current();
    const originalSize = currentDomain.length;
    const removedValues = new Set<number>();
    const assignedVars = new Set<Variable>();
    sudoku.variables.forEach(var1 => {
      var1.domain.startTemp();
      if (var1.isAssigned()) {
        assignedVars.add(var1);
      }
    });
    for (let i = originalSize - 1; i >= 0; i--) {
      variable.assign(currentDomain[i]);
      // Now to run temp ac3
      const acqueue = new Queue<[Variable, Variable]>();
      sudoku.variables.forEach(var1 =>
        var1.domain.resetTemp()
      );
      variable.binaryNeighbors.forEach(neighbor =>
        acqueue.push([neighbor, variable])
      );
      while (!acqueue.isEmpty()) {
        const tuple = acqueue.pop();
        const original = tuple[0].domain.current();
        if (tuple[0].binaryRevise(tuple[1])) {
          tuple[0].binaryNeighbors.forEach(neighbor =>
            acqueue.push([neighbor, tuple[0]])
          );
        }
      }
      // Undo assignment
      const value = variable.getAssignment();
      variable.unassign();
      // If ac3 detects any inconsistencies, current value cannot be part of a solution
      for (let j = 0; j < sudoku.variables.length; j++) {
        if (sudoku.variables[j].domain.size() === 0) {
          removedValues.add(value);
          break;
        }
      }
    }
    sudoku.variables.forEach(var1 => {
      var1.domain.endTemp();
    });

    totalRemoved.initialize(variable);
    const toRemove = new ValuesRemoved();
    toRemove.initialize(variable);
    removedValues.forEach(removedValue => {
      totalRemoved.addRemoved(variable, removedValue);
      toRemove.addRemoved(variable, removedValue);
      variable.domain.remove(removedValue);
      if (variable.domain.size() === 0) {
        consistent = false;
      }
    });
    if (originalSize !== variable.domain.size()) {
      const acremoved = sudoku.binaryArcConsistency();
      acremoved.removeHistory.forEach(valuesRemoved => {
        valuesRemoved.forEach((v, removed) => {
          totalRemoved.initialize(v);
          toRemove.initialize(v);
          removed.forEach(removedValue => {
            totalRemoved.addRemoved(v, removedValue);
            toRemove.addRemoved(v, removedValue);
          });
          v.binaryNeighbors.forEach(neighbor => {
            queue.push(neighbor);
          });
        });
      });

      removeHistory.push(toRemove);
      variable.binaryNeighbors.forEach(neighbor =>
        queue.push(neighbor)
      );
      consistent = consistent && acremoved.consistent;
    }
  }
  return { removeHistory, consistent };
};

export default SingletonArcConsistency;
