import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import ValuesRemoved from 'structures/ValuesRemoved';
import IConsistencyOutput from 'structures/IConsistencyOutput';
import Queue from 'structures/Queue';
import Sudoku from 'Sudoku';
import Variable from 'Variable';

const SingletonGeneralArcConsistency = (
  sudoku: Sudoku,
  specific?: IGeneralConstraint
): IConsistencyOutput => {
  const generalOutput = sudoku.generalArcConsistency();
  if (!generalOutput.consistent) {
    return generalOutput;
  }
  const removeHistory = generalOutput.removeHistory;
  let consistent = true;
  const totalRemoved = new ValuesRemoved();
  const queue = new Queue<Variable>();
  sudoku.variables.forEach(variable => {
    if (variable.domain.size() !== 1) {
      queue.push(variable);
    }
  });
  while (!queue.isEmpty() && consistent) {
    const variable = queue.pop();
    if (variable.domain.size() === 1) {
      continue;
    }
    const currentDomain = variable.domain.current();
    const originalSize = currentDomain.length;
    const removedValues = new Set<number>();
    sudoku.variables.forEach(var1 =>
      var1.domain.startTemp()
    );
    const toRemove = new ValuesRemoved();
    for (let i = originalSize - 1; i >= 0; i--) {
      variable.assign(currentDomain[i]);
      sudoku.variables.forEach(var1 =>
        var1.domain.resetTemp()
      );
      // AC3
      const acqueue = new Queue<[Variable, Variable]>();
      const acset = new Set<Variable>([variable]);
      variable.binaryNeighbors.forEach(neighbor =>
        acqueue.push([neighbor, variable])
      );
      while (!acqueue.isEmpty()) {
        const tuple = acqueue.pop();
        const original = tuple[0].domain.current();
        if (tuple[0].binaryRevise(tuple[1])) {
          tuple[0].binaryNeighbors.forEach(neighbor =>
            acqueue.push([neighbor, tuple[0]])
          );
          acset.add(tuple[0]);
        }
      }

      // GAC
      if (specific != null) {
        specific.getInconsistent();
      } else {
        const gacqueue = new Queue<IGeneralConstraint>();

        acset.forEach(v => {
          for (let j = 0; j < v.generalConstraints.length; j++) {
            gacqueue.push(v.generalConstraints[j]);
          }
        });
        while (!gacqueue.isEmpty()) {
          const constraint = gacqueue.pop();
          const inconsistent = constraint.getInconsistent();
          inconsistent.forEach((v, removed) => {
            removed.forEach(removedValue =>
              v.domain.remove(removedValue)
            );
            for (let j = 0; j < v.generalConstraints.length; j++) {
              if (v.generalConstraints[j] !== constraint && !gacqueue.contains(v.generalConstraints[j])) {
                gacqueue.push(v.generalConstraints[j]);
              }
            }
          });
        }
      }

      const removal = variable.getAssignment();
      // Undo assignment
      variable.unassign();
      // If gac detects any inconsistencies, current value cannot be part of a solution
      for (const var1 of sudoku.variables) {
        if (var1.domain.size() === 0) {
          variable.domain.endTemp();
          variable.domain.remove(removal);
          toRemove.initialize(variable);
          toRemove.addRemoved(variable, removal);
          variable.domain.startTemp();
          removedValues.add(removal);
          break;
        }
      }
    }
    sudoku.variables.forEach(var1 => {
      var1.domain.endTemp();
    });
    totalRemoved.initialize(variable);
    removedValues.forEach(removedValue => {
      totalRemoved.addRemoved(variable, removedValue);
      variable.domain.remove(removedValue);
      if (variable.domain.size() === 0) {
        consistent = false;
      }
    });
    if (originalSize !== variable.domain.size()) {
      const gacremoved = sudoku.generalArcConsistency();
      gacremoved.removeHistory.forEach(valuesRemoved => {
        valuesRemoved.forEach((v, removed) => {
          totalRemoved.initialize(v);
          toRemove.initialize(v);
          removed.forEach(removedValue => {
            totalRemoved.addRemoved(v, removedValue);
            toRemove.addRemoved(v, removedValue);
          });
          v.binaryNeighbors.forEach(neighbor => {
            queue.push(neighbor);
          });
        });
      });

      removeHistory.push(toRemove);
      variable.binaryNeighbors.forEach(neighbor => {
        queue.push(neighbor);
      });

      consistent = consistent && gacremoved.consistent;
    }
  }
  return { removeHistory, consistent };
};

export default SingletonGeneralArcConsistency;
