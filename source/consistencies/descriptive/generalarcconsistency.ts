import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import ValuesRemoved from 'structures/ValuesRemoved';
import PayloadQueue from 'structures/PayloadQueue';
import IConsistencyOutput from 'structures/IConsistencyOutput';
import Sudoku from 'Sudoku';
import Variable from 'Variable';

const GeneralArcConsistency = (
  sudoku: Sudoku,
  specific?: IGeneralConstraint
): IConsistencyOutput => {
  const totalRemoved = new ValuesRemoved();
  const removeHistory = new Array<ValuesRemoved>();
  let consistent = true;
  if (specific) {
    const inconsistent = specific.getInconsistent();
    removeHistory.push(inconsistent);
    inconsistent.forEach((variable, removed) => {
      removed.forEach(removedValue => {
        totalRemoved.addRemoved(variable, removedValue);
        variable.domain.remove(removedValue);
        if (variable.domain.size() === 0) {
          consistent = false;
        }
      });
    });
  } else {
    const payloadQueue = new PayloadQueue<IGeneralConstraint, ValuesRemoved>();
    sudoku.generalConstraints.forEach(constraint => {
      constraint.resetPropagation();
      payloadQueue.push(constraint, new ValuesRemoved());
    });
    while (!payloadQueue.isEmpty() && consistent) {
      const [constraint, valuesRemoved] = payloadQueue.pop();
      const { valid, reduction } = constraint.diffPropagation(valuesRemoved);
      if (!valid) {
        consistent = false;
      }
      if (!reduction.isEmpty()) {
        removeHistory.push(reduction);
      }
      reduction.forEach((variable, removedValues) => {
        totalRemoved.initialize(variable);
        removedValues.forEach(removedValue => {
          if (variable.isAssigned() || !variable.domain.remove(removedValue)) {
            consistent = false;
          } else {
            totalRemoved.addRemoved(variable, removedValue);
            if (variable.domain.size() === 0) {
              consistent = false;
            }
          }
        });
        variable.generalConstraints.forEach(neighborConstraint => {
          if (neighborConstraint !== constraint) {
            if (payloadQueue.contains(neighborConstraint)) {
              payloadQueue.get(neighborConstraint).addAllRemoved(variable, removedValues);
            } else {
              payloadQueue.push(
                neighborConstraint,
                new ValuesRemoved({
                  [variable.id]: {
                    variable,
                    removed: removedValues,
                  },
                })
              );
            }
          }
        });
      });
    }
  }
  return { removeHistory, consistent };
};

export default GeneralArcConsistency;
