import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import ValuesRemoved from 'structures/ValuesRemoved';
import IConsistencyOutput from 'structures/IConsistencyOutput';
import Queue from 'structures/Queue';
import Variable from 'Variable';

// The first variable in the set must be the modified variable
const GeneralForwardChecking = (
  set: IGeneralConstraint[]
): IConsistencyOutput => {
  const removedTotal = new ValuesRemoved();
  const removeHistory = new Array<ValuesRemoved>();
  const queue = new Queue<IGeneralConstraint>();
  let consistent = true;
  set.forEach(generalConstraint => queue.push(generalConstraint));

  while (!queue.isEmpty()) {
    const generalConstraint = queue.pop();
    const inconsistent = generalConstraint.getInconsistent();
    inconsistent.forEach((variable, removed) => {
      removed.forEach(removedValue => {
        variable.domain.remove(removedValue);
        removedTotal.addRemoved(variable, removedValue);
        if (variable.domain.size() === 0) {
          consistent = false;
        }
      });

      set.forEach(constraint => {
        if (constraint !== generalConstraint
          && constraint.scope.find(v => v === variable) !== undefined) {
            queue.push(constraint);
          }
        }
      );
    });
    if (!inconsistent.isEmpty()) {
      removeHistory.push(inconsistent);
    }
  }
  return { removeHistory, consistent };
};

export default GeneralForwardChecking;
