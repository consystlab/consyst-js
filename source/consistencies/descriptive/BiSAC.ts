import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import ValuesRemoved from 'structures/ValuesRemoved';
import IConsistencyOutput from 'structures/IConsistencyOutput';
import SparseSet from 'domains/SparseSet';
import IDomain from 'domains/IDomain';
import Queue from 'structures/Queue';
import Sudoku from 'Sudoku';
import Variable from 'Variable';

const startingSizes: Array<Array<[Variable, number]>> = [];

const saveDomains = (variables: Array<Variable>, level: number) => {
  startingSizes[level] = [];
  for (let i = 0; i < variables.length; i++) {
    startingSizes[level].push([variables[i], variables[i].domain.size()]);
  }
};

const restoreDomains = (level: number) => {
  for (let i = 0; i < startingSizes[level].length; i++) {
    (startingSizes[level][i][0].domain as SparseSet).restoreRange(
      startingSizes[level][i][1] - startingSizes[level][i][0].domain.size()
    );
  }
};

const bisacAC3 = (startingVariable: Variable, vvp: [Variable, number]) => {
  let consistent = true;
  const queue = new Queue<[Variable, Variable]>();
  startingVariable.binaryNeighbors.forEach(var2 => {
    queue.push([var2, startingVariable]);
  });
  while (!queue.isEmpty()) {
    const tuple = queue.pop();
    if (tuple[0].binaryRevise(tuple[1])) {
      tuple[0].binaryNeighbors.forEach(neighbor => {
        queue.push([neighbor, tuple[0]]);
      });

      if (!tuple[0].domain.size() || !vvp[0].domain.contains(vvp[1])) {
        consistent = false;
        break;
      }
    }
  }
  return consistent;
};

const BiSAC = (
  sudoku: Sudoku,
  subset?: Variable[]
): IConsistencyOutput => {
  const singletonConsistency = sudoku.singletonArcConsistency(subset);
  if (!singletonConsistency.consistent) {
    return singletonConsistency;
  }
  const removeHistory = singletonConsistency.removeHistory;

  let consistent = true;
  const queue = new Queue<Variable>();
  sudoku.variables.forEach(variable => {
    if (!variable.isAssigned() && variable.domain.size() > 1) {
      queue.push(variable);
    }
  });
  let change = true;
  while (change && consistent) {
    change = false;
    for (let i = 0; i < sudoku.variables.length; i++) {
      const variable1 = sudoku.variables[i];
      const domain1 = variable1.domain.current();
      const toRemove = new ValuesRemoved();
      for (let j = 0; j < domain1.length; j++) {
        const domainRemoval: Array<[Variable, number]> = [];
        for (let k = 0; k < sudoku.variables.length; k++) {
          if (i === k) {
            continue;
          }
          const variable2 = sudoku.variables[k];
          if (variable2.domain.size() === 1 || variable2.isAssigned()) {
            continue;
          }
          const domain2 = variable2.domain.current();
          for (let p = 0; p < domain2.length; p++) {
            variable2.assign(domain2[p]);
            saveDomains(sudoku.variables, 1);
            const valid = bisacAC3(variable2, [variable1, domain1[j]]);
            restoreDomains(1);
            variable2.unassign();
            if (!valid) {
              domainRemoval.push([variable2, domain2[p]]);
            }
          }
        }
        saveDomains(sudoku.variables, 0);
        for (let k = 0; k < domainRemoval.length; k++) {
          domainRemoval[k][0].domain.remove(domainRemoval[k][1]);
        }

        const overallRun = sudoku.binaryArcConsistency();

        restoreDomains(0);

        if (!overallRun.consistent) {
          toRemove.addRemoved(variable1, domain1[j]);
          variable1.domain.remove(domain1[j]);
          change = true;
          if (!variable1.domain.size()) {
            removeHistory.push(toRemove);
            consistent = false;
            return { removeHistory, consistent };
          }
        }
      }
      if (!toRemove.isEmpty()) {
        removeHistory.push(toRemove);
      }
      if (variable1.domain.size() === 1) {
        const outerRun = sudoku.singletonArcConsistency();
        const sacRemoved = new ValuesRemoved();
        outerRun.removeHistory.forEach(valuesRemoved => {
          valuesRemoved.forEach((v, removed) => {
            removed.forEach(removedValue => {
              sacRemoved.addRemoved(v, removedValue);
            });
            v.binaryNeighbors.forEach(neighbor => {
              queue.push(neighbor);
            });
          });
          consistent = consistent && outerRun.consistent;
        });
        if (!sacRemoved.isEmpty()) {
          removeHistory.push(sacRemoved);
        }
      }
    }
  }
  return { removeHistory, consistent };
};

export default BiSAC;
