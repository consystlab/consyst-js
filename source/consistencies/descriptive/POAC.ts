import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import ValuesRemoved from 'structures/ValuesRemoved';
import IConsistencyOutput from 'structures/IConsistencyOutput';
import SparseSet from 'domains/SparseSet';
import IDomain from 'domains/IDomain';
import Queue from 'structures/Queue';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import CyclicList from 'structures/CyclicList';

let counter: { [key: string]: Array<number> };

const restoreDomains = (removal: ValuesRemoved, update: boolean) => {
  if (update) {
    removal.forEach((variable, removed ) => {
      removed.forEach(value => {
        variable.domain.restore(value);
        if (!counter[variable.id]) {
          counter[variable.id] = [];
        }
        if (!counter[variable.id][value]) {
          counter[variable.id][value] = 0;
        }
        counter[variable.id][value]++;
      });
    });
  } else {
    removal.forEach((variable, removed) => {
      removed.forEach(value => {
        variable.domain.restore(value);
      });
    });
  }
};

const testAC = (
  startingVariable: Variable,
): {
  consistent: boolean,
} => {
  const queue = new Queue<[Variable, Variable]>();
  startingVariable.binaryNeighbors.forEach(neighbor => {
    queue.push([neighbor, startingVariable]);
  });
  const removal = new ValuesRemoved();
  while (!queue.isEmpty()) {
    const tuple = queue.pop();
    const size = tuple[0].domain.size();
    const original = tuple[0].domain.current();
    for (let i = 0; i < original.length; i++) {
      if (!tuple[0].binarySupported(original[i], tuple[1])) {
        removal.addRemoved(tuple[0], original[i]);
        tuple[0].domain.remove(original[i]);
      }
    }
    if (tuple[0].domain.size() === 0) {
      restoreDomains(removal, false);
      return {
        consistent: false,
      };
    }
    if (tuple[0].domain.size() < size) {
      tuple[0].binaryNeighbors.forEach(neighbor => {
        queue.push([neighbor, tuple[0]]);
      });
    }
  }
  restoreDomains(removal, true);
  return {
    consistent: true,
  };
};

const varPOAC = (
  startingVariable: Variable,
  sudoku: Sudoku,
): { consistent: boolean, change: boolean, removals: ValuesRemoved[] } => {
  let size = startingVariable.domain.size();
  let change = false;
  const removals = new Array<ValuesRemoved>();

  counter = {};
  let domain = startingVariable.domain.current();
  for (let i = 0; i < domain.length; i++) {
    startingVariable.assign(domain[i]);
    const test = testAC(startingVariable);
    startingVariable.unassign();
    if (!test.consistent) {
      startingVariable.domain.remove(domain[i]);
      removals.push(new ValuesRemoved({ [startingVariable.id]: {
        variable: startingVariable,
        removed: new Set([domain[i]]),
      }}));
      const enforce = sudoku.binaryArcConsistency();
      removals.push.apply(removals, enforce.removeHistory);
      if (!enforce.consistent) {
        return {
          consistent: false,
          change: true,
          removals,
        };
      }
    }
  }
  if (startingVariable.domain.size() === 0) {
    return {
      consistent: false,
      change: true,
      removals,
    };
  }
  if (size !== startingVariable.domain.size()) {
    change = true;
  }
  const symmetricRemoval = new ValuesRemoved();
  for (let i = 0; i < sudoku.variables.length; i++) {
    const variable = sudoku.variables[i];
    if (variable === startingVariable) {
      continue;
    }
    size = variable.domain.size();
    domain = variable.domain.current();
    for (let j = 0; j < domain.length; j++) {
      const c = (counter[variable.id] && counter[variable.id][domain[j]]) || 0;
      if (c === startingVariable.domain.size()) {
        variable.domain.remove(domain[j]);
        symmetricRemoval.addRemoved(variable, domain[j]);
      }
    }
    if (variable.domain.size() === 0) {
      removals.push(symmetricRemoval);
      return {
        consistent: false,
        change,
        removals,
      };
    }
    if (variable.domain.size() !== size) {
      change = true;
    }
  }
  if (!symmetricRemoval.isEmpty()) {
    removals.push(symmetricRemoval);
  }
  return {
    consistent: true,
    change,
    removals,
  };
};

const POAC1 = (
  sudoku: Sudoku
): IConsistencyOutput => {
  const arcConsistency = sudoku.binaryArcConsistency();
  if (!arcConsistency.consistent) {
    return arcConsistency;
  }
  const removeHistory = arcConsistency.removeHistory;
  let consistent = true;
  const s = new CyclicList(sudoku.variables);

  let fpp = 0;
  let x = s.next();
  const cycleLength = s.length;
  while (fpp < cycleLength && consistent) {
    let change = false;
    if (!x.isAssigned() && x.domain.size() > 1) {
      const varPoacRun = varPOAC(x, sudoku);
      consistent = varPoacRun.consistent;
      change = varPoacRun.change;
      removeHistory.push.apply(removeHistory, varPoacRun.removals);
      if (!consistent) {
        break;
      }
    }
    if (change) {
      fpp = 1;
    } else {
      fpp++;
    }
    x = s.next();
  }

  return {
    removeHistory,
    consistent,
  };
};

export default POAC1;
