import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import ValuesRemoved from 'structures/ValuesRemoved';
import Queue from 'structures/Queue';
import IConsistencyOutput from 'structures/IConsistencyOutput';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import BinaryTableSparse from 'domains/BinaryTableSparse';
import SparseSet from 'domains/SparseSet';
import GeneralArcConsistency from 'consistencies/descriptive/generalarcconsistency';

const Singleton2GeneralArcConsistency = (
  sudoku: Sudoku,
  specific?: IGeneralConstraint
): IConsistencyOutput => {
  const totalRemoved = new ValuesRemoved();
  const singletonOutput = sudoku.singletonGeneralArcConsistency();
  if (!singletonOutput.consistent) {
    return singletonOutput;
  }
  const removeHistory = singletonOutput.removeHistory;
  const queue = new Queue<BinaryTableSparse>();
  const relations: { [key: string]:
    { [key: string]: BinaryTableSparse }
  } = {};
  const domainSizes: { [key: string]: number } = {};
  for (let i = 0; i < sudoku.variables.length; i++) {
    relations[sudoku.variables[i].toString()] = {};
    domainSizes[sudoku.variables[i].toString()] = sudoku.variables[i].domain.size();
    for (let j = i + 1; j < sudoku.variables.length; j++) {
      if (!relations[sudoku.variables[j].toString()]) {
        relations[sudoku.variables[j].toString()] = {};
      }
      const binaryTable = new BinaryTableSparse([sudoku.variables[i], sudoku.variables[j]]);
      queue.push(binaryTable);
      relations[sudoku.variables[i].toString()][sudoku.variables[j].toString()] = binaryTable;
      relations[sudoku.variables[j].toString()][sudoku.variables[i].toString()] = binaryTable;
    }
  }

  while (!queue.isEmpty()) {
    const relation = queue.pop();
    const scope = relation.scope;
    const relationList = relation.current();
    const originalRelationSize = relation.size();
    const removedValues = {
      [scope[0].toString()]: new Set<number>(),
      [scope[1].toString()]: new Set<number>(),
    };
    const toRemove = new ValuesRemoved();

    for (let i = originalRelationSize - 1; i >= 0; i--) {
      scope[0].assign(relationList[i][0]);
      scope[1].assign(relationList[i][1]);

      // GAC
      const gacqueue = new Queue<IGeneralConstraint>();

      for (let j = 0; j < scope[0].generalConstraints.length; j++) {
        gacqueue.push(scope[0].generalConstraints[j]);
      }
      for (let j = 0; j < scope[1].generalConstraints.length; j++) {
        gacqueue.push(scope[1].generalConstraints[j]);
      }

      let domainWipeout = false;

      while (!gacqueue.isEmpty() && !domainWipeout) {
        const constraint = gacqueue.pop();
        const inconsistent = constraint.getInconsistent();
        inconsistent.forEach((v, removed) => {
          removed.forEach(removedValue => {
            remove(v, removedValue);
            if (!v.domain.size()) {
              domainWipeout = true;
            }
          });
          for (let j = 0; j < v.generalConstraints.length; j++) {
            if (v.generalConstraints[j] !== constraint && !gacqueue.contains(v.generalConstraints[j])) {
              gacqueue.push(v.generalConstraints[j]);
            }
          }
        });
      }

      // Undo assignment
      scope[0].unassign();
      scope[1].unassign();
      for (let j = 0; j < sudoku.variables.length; j++) {
        const diff = domainSizes[sudoku.variables[j].toString()] - sudoku.variables[j].domain.size();
        (sudoku.variables[j].domain as SparseSet).restoreRange(diff);
      }

      if (domainWipeout) {
        relation.remove(relationList[i]);
        if (!relation.supported(scope[0], relationList[i][0])) {
          removedValues[scope[0].toString()].add(relationList[i][0]);
        }
        if (!relation.supported(scope[1], relationList[i][1])) {
          removedValues[scope[1].toString()].add(relationList[i][1]);
        }
      }
    }

    const removedStep = new ValuesRemoved();
    if (removedValues[scope[0].toString()].size !== 0) {
      removedStep.initialize(scope[0]);
      removedStep.addAllRemoved(scope[0], removedValues[scope[0].toString()]);
      totalRemoved.initialize(scope[0]);
      removedValues[scope[0].toString()].forEach(removedValue => {
        totalRemoved.addRemoved(scope[0], removedValue);
        scope[0].domain.remove(removedValue);
        domainSizes[scope[0].toString()]--;
      });
    }

    if (removedValues[scope[1].toString()].size !== 0) {
      removedStep.initialize(scope[1]);
      removedStep.addAllRemoved(scope[1], removedValues[scope[1].toString()]);
      totalRemoved.initialize(scope[1]);
      removedValues[scope[1].toString()].forEach(removedValue => {
        totalRemoved.addRemoved(scope[1], removedValue);
        scope[1].domain.remove(removedValue);
        domainSizes[scope[1].toString()]--;
      });
    }

    if (!removedStep.isEmpty()) {
      const gacremoved = sudoku.generalArcConsistency();
      gacremoved.removeHistory.forEach(valuesRemoved => {
        valuesRemoved.forEach((v, removed) => {
          removedStep.initialize(v);
          removedStep.addAllRemoved(v, removed);

          totalRemoved.initialize(v);
          toRemove.initialize(v);
          removed.forEach(removedValue => {
            totalRemoved.addRemoved(v, removedValue);
            toRemove.addRemoved(v, removedValue);
          });
        });
      });

      removeHistory.push(toRemove);
      removedStep.forEach((variable) => {
          variable.binaryNeighbors.forEach(neighbor => {
          queue.push(relations[variable.toString()][neighbor.toString()]);
        });
      });
    }

  }
  return { removeHistory, consistent: true };
};

const assign = (variable: Variable, value: number) => {
  variable.domain.current().forEach(removal => {
    if (removal !== value) {
      variable.generalConstraints.forEach(c => c.remove(variable, removal));
    }
  });
  variable.assign(value);
};

const unassign = (variable: Variable) => {
  const assignment = variable.domain.first();
  variable.unassign();
  variable.domain.current().forEach(value => {
    if (value !== assignment) {
      variable.generalConstraints.forEach(c => c.restore(variable, value));
    }
  });
};

const remove = (variable: Variable, value: number): void => {
  variable.generalConstraints.forEach(c => c.remove(variable, value));
  variable.domain.remove(value);
};

const restore = (variable: Variable, value: number): void => {
  variable.generalConstraints.forEach(c => c.restore(variable, value));
  variable.domain.restore(value);
};

const restoreAll = (variable: Variable): void => {
  variable.domain.restore();
  variable.domain.current().forEach(value =>
    variable.generalConstraints.forEach(c => c.restore(variable, value))
  );
};

export default Singleton2GeneralArcConsistency;
