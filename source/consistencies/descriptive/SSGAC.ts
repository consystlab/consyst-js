import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import ValuesRemoved from 'structures/ValuesRemoved';
import IConsistencyOutput from 'structures/IConsistencyOutput';
import Queue from 'structures/Queue';
import Sudoku from 'Sudoku';
import Variable from 'Variable';
import SparseSet from 'domains/SparseSet';

const SSGAC = (
  sudoku: Sudoku,
): IConsistencyOutput => {
  const initialSgacOutput = sudoku.singletonGeneralArcConsistency();
  if (!initialSgacOutput.consistent) {
    return initialSgacOutput;
  }
  const removeHistory = initialSgacOutput.removeHistory;
  let consistent = true;

  const totalRemoved = new ValuesRemoved();
  const queue = new Queue<Variable>();
  sudoku.variables.forEach(variable => {
    if (variable.domain.size() !== 1) {
      queue.push(variable);
    }
  });
  while (!queue.isEmpty() && consistent) {
    const variable = queue.pop();
    if (variable.domain.size() === 1) {
      continue;
    }
    const currentDomain = variable.domain.current();
    const originalSize = currentDomain.length;
    const removedValues = new Set<number>();
    const startingSizes = [];
    for (let i = 0; i < sudoku.variables.length; i++) {
      startingSizes.push([sudoku.variables[i], sudoku.variables[i].domain.size()] as [Variable, number]);
    }

    const toRemove = new ValuesRemoved();
    for (let i = originalSize - 1; i >= 0; i--) {
      const assignment = currentDomain[i];
      variable.assign(assignment);
      for (let j = 0; j < startingSizes.length; j++) {
        (startingSizes[j][0].domain as SparseSet).restoreRange(startingSizes[j][1] - startingSizes[j][0].domain.size());
      }

      const sgacOutput = sudoku.singletonGeneralArcConsistency();

      variable.unassign();
      // If gac detects any inconsistencies, current value cannot be part of a solution
      if (!sgacOutput.consistent) {
        toRemove.initialize(variable);
        toRemove.addRemoved(variable, assignment);
        removedValues.add(assignment);
      }
    }

    for (let i = 0; i < startingSizes.length; i++) {
      (startingSizes[i][0].domain as SparseSet).restoreRange(startingSizes[i][1] - startingSizes[i][0].domain.size());
    }

    totalRemoved.initialize(variable);
    removedValues.forEach(removedValue => {
      totalRemoved.addRemoved(variable, removedValue);
      variable.domain.remove(removedValue);
      if (variable.domain.size() === 0) {
        consistent = false;
      }
    });
    if (originalSize !== variable.domain.size()) {
      const sgacRemoved = sudoku.singletonGeneralArcConsistency();
      sgacRemoved.removeHistory.forEach(valuesRemoved => {
        valuesRemoved.forEach((v, removed) => {
          totalRemoved.initialize(v);
          toRemove.initialize(v);
          removed.forEach(removedValue => {
            totalRemoved.addRemoved(v, removedValue);
            toRemove.addRemoved(v, removedValue);
          });
          v.binaryNeighbors.forEach(neighbor => {
            queue.push(neighbor);
          });
        });
      });

      removeHistory.push(toRemove);
      variable.binaryNeighbors.forEach(neighbor => {
        queue.push(neighbor);
      });

      consistent = consistent && sgacRemoved.consistent;
    }
  }
  return { removeHistory, consistent };
};

export default SSGAC;
