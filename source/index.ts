import IBinaryConstraint from 'constraints/binary/IBinaryConstraint';
import BinaryAllDiff from 'constraints/binary/BinaryAllDiff';
import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import GeneralAllDiff from 'constraints/general/GeneralAllDiff';
import ValueGraph from 'constraints/general/util/ValueGraph';
import {
  Propagations,
  SearchResults,
} from 'Enumerations';
import Queue from 'structures/Queue';
import SparseSet from 'domains/SparseSet';
import Sudoku from 'Sudoku';
import ValuesRemoved from 'structures/ValuesRemoved';
import Variable from 'Variable';
import VariableValuePair from 'structures/VariableValuePair';
import VariableValuePairs from 'structures/VariableValuePairs';
import IDomain from 'domains/IDomain';
import BinaryTableSparse from 'domains/BinaryTableSparse';

/**
 *  Consistency Algorithms
 */
import BinaryArcConsistency from 'consistencies/descriptive/binaryarcconsistency';
import BinaryForwardChecking from 'consistencies/descriptive/binaryforwardchecking';
import GeneralArcConsistency from 'consistencies/descriptive/generalarcconsistency';
import SingletonArcConsistency from 'consistencies/descriptive/singletonarcconsistency';
import SingletonGeneralArcConsistency from 'consistencies/descriptive/singletongeneralarcconsistency';
import SSGAC from 'consistencies/descriptive/SSGAC';
import GAC from 'consistencies/performant/GAC';

/**
 *  Search Algorithms
 */
import BCSSP from 'searches/bcssp';
import FC from 'searches/fc';
import FCGAC from 'searches/fcGac';
import SearchAnalytics from 'structures/SearchAnalytics';

/**
 *  Variable Ordering Heuristics
 */
import DomWDegOrdering from 'orderings/variable/DomWDegOrdering';
import LexicographicalOrdering from 'orderings/variable/LexicographicalOrdering';
import LexicoSingletonOrdering from 'orderings/variable/LexicoSingletonOrdering';
import VariableOrdering from 'orderings/variable/VariableOrdering';

const myExports = {
  BinaryAllDiff,
  GeneralAllDiff,
  Propagations,
  SearchResults,
  Queue,
  SparseSet,
  Sudoku,
  ValueGraph,
  ValuesRemoved,
  Variable,
  VariableValuePair,
  VariableValuePairs,
  SearchAnalytics,

  BinaryArcConsistency,
  BinaryForwardChecking,
  GeneralArcConsistency,
  SingletonArcConsistency,
  SingletonGeneralArcConsistency,
  SSGAC,

  BCSSP,
  FC,
  FCGAC,

  VariableOrdering,
  LexicographicalOrdering,
  LexicoSingletonOrdering,
  DomWDegOrdering,
};

Object.assign(myExports, { default: Object.assign({}, myExports) });
export = myExports;
