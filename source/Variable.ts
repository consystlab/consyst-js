import IBinaryConstraint from 'constraints/binary/IBinaryConstraint';
import IGeneralConstraint from 'constraints/general/IGeneralConstraint';
import IDomain from 'domains/IDomain';
import VariableValuePairs from 'structures/VariableValuePairs';

export default class Variable {
  public static copy(variable: Variable): Variable {
    const copiedVariable = new Variable(variable.domain.copy(), variable.id);
    if (variable.isAssigned()) {
      copiedVariable.assign(variable.getAssignment());
    }
    return copiedVariable;
  }

  public binaryNeighbors: Set<Variable>;
  public generalConstraints: IGeneralConstraint[];
  public binaryConstraints: Set<IBinaryConstraint>;

  private binaryConstraintMap: { [key: string]: Set<IBinaryConstraint>; };
  private assignment: number;

  constructor(public domain: IDomain, public id: string) {
    this.binaryNeighbors = new Set<Variable>();
    this.binaryConstraintMap = {};
    this.binaryConstraints = new Set<IBinaryConstraint>();
    this.generalConstraints = new Array<IGeneralConstraint>();
    this.assignment = null;
  }

  public toString(): string {
    return this.id;
  }

  public addBinaryConstraint(c: IBinaryConstraint): void {
    this.binaryConstraints.add(c);
    const otherVariable = this.otherBinaryVariable(c);
    if (!this.binaryConstraintMap[otherVariable.id]) {
      this.binaryConstraintMap[otherVariable.id] = new Set<IBinaryConstraint>();
    }
    this.binaryConstraintMap[otherVariable.id].add(c);
    this.binaryNeighbors.add(otherVariable);
  }

  public addGeneralConstraint(c: IGeneralConstraint): void {
    this.generalConstraints.push(c);
  }

  public binarySupported(value: number, variable: Variable): boolean {
    let supported = true;
    if (!this.binaryNeighbors.has(variable)) {
      return true;
    }
    if (variable.domain.size() === 1 || variable.isAssigned()) {
      const otherValue = variable.isAssigned() ? variable.getAssignment() : variable.domain.first();
      this.binaryConstraintMap[variable.id].forEach(constraint => {
        if (!constraint.check(
          new VariableValuePairs([
            { key: this, value },
            { key: variable, value: otherValue },
          ])
        )) {
          supported = false;
        }
      });
    }
    return supported;
  }

  public binaryRevise(variable: Variable): boolean {
    if (this.isAssigned()) {
      return false;
    }
    const originalDomain = this.domain.current();
    const originalSize = this.domain.size();
    originalDomain.forEach(elem => {
      if (!this.binarySupported(elem, variable)) {
        this.domain.remove(elem);
      }
    });
    return this.domain.size() !== originalSize;
  }
  public assign(value: number): boolean {
    if (this.assignment !== null) {
      return false;
    }
    if (!this.domain.contains(value)) {
      return false;
    }
    this.assignment = value;
    return true;
  }

  public unassign(): void {
    this.assignment = null;
  }

  public isAssigned(): boolean {
    return this.assignment !== null;
  }

  public getAssignment(): number {
    return this.assignment;
  }

  private otherBinaryVariable(c: IBinaryConstraint): Variable {
    if (this.id === c.scope[0].id) {
      return c.scope[1];
    }
    return c.scope[0];
  }
}
